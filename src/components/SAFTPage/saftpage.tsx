
import {
	useContext,
	useEffect,
	useRef,
	useState
} from "react"
import {
	matchRoutes,
	useLocation,
} from "react-router-dom";
import Dropzone from "react-dropzone";
import {
	BigNumber,
	ChainType,
	CollateralItem,
	ERC20Type,
	LockType,
	NFT,
	RoyaltyInput,
	Rules,
	SAFTRecipientItem,
	Web3,
	_AssetType,
	_Asset,
	addThousandSeparator,
	assetTypeToString,
	checkApprovalERC1155,
	checkApprovalERC721,
	checkApprovalForAllERC721,
	compactString,
	decodeRules,
	getChainId,
	getContractNameERC721,
	getERC20BalanceFromChain,
	getNullERC20,
	getUserNFTsOfContractFromAPI,
	getWNFTById,
	getWrapperTechToken,
	isAddress,
	localStorageGet,
	localStorageRemove,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	removeThousandSeparator,
	setApprovalERC1155,
	setApprovalERC1155Multisig,
	setApprovalERC721,
	setApprovalERC721Multisig,
	setApprovalForAllERC721,
	setApprovalForAllERC721Multisig,
	tokenToFloat,
	tokenToInt,
	combineURLs,
	getWrapperOfBatchWorker,
	chainTypeToERC20,
	addDays,
	dateToStrWithMonth,
	dateToUnixtime,
	decodeAssetTypeFromString,
	decodeAssetTypeFromIndex,
	getWrapperLastMinted,
} from "@envelop/envelop-client-core";

import TippyWrapper        from "../TippyWrapper";
import NumberInput         from "../NumberInput";
import CoinSelector        from "../CoinSelector";
import CollateralViewer    from "../CollateralViewer";
import InputWithOptions    from "../InputWithOptions";
import CollateralSummary   from "../CollateralSummary";
import AddCollateralPage   from "../AddCollateralPage";
import MintPage            from "../MintPage";
import CopyToClipboard     from "react-copy-to-clipboard";

import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	SubscriptionContext,
	SubscriptionRenderer,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../dispatchers";

import {
	getBlacklist,
	getMaxCollaterals,
	getWhitelist,
	getWrapperWhitelist,
	isEnabledForCollateral,
	isEnabledForFee,
	isEnabledRemoveFromCollateral
} from "../../models";

import icon_i_del            from '../../static/pics/i-del.svg';
import icon_external         from '../../static/pics/icons/i-external.svg';
import icon_i_attention      from '../../static/pics/i-attention.svg';
import i_copy                from '../../static/pics/icons/i-copy.svg';

import config                from '../../app.config.json';

import {
	deployUserBatchWorker,
	deployUserBatchWorkerMultisig,
	getUserBatchWrappers,
	getWhitelistItem,
	saftToken,
	saftTokenMultisig,
	saftTokenOnUserContract,
	saftTokenOnUserContractMultisig,
} from "../../models/saft";

type FetchedTokensOfContractType = {
	isLoading: boolean,
	contractAddress: string,
	collectionName: string,
	tokens: Array<NFT>,
	badContract: boolean,
}

export default function SAFTPage() {

	let scrollable: any = {
		originalToken: useRef(null),
		timelock: useRef(null),
		standart: useRef(null),
		advancedOptions: useRef(null),
		collaterals: useRef(null),
		royalty: useRef(null),
		WNFTRecipients: useRef(null),
		recipients: useRef(null),
	};

	const {
		userAddress,
		currentChain,
		web3,
		getWeb3Force,
		balanceNative
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token
	} = useContext(ERC20Context);
	const {
		setModal,
		setError,
		setInfo,
		unsetModal,
		setLoading,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		subscriptionRemainings,
		updateTicket,
		subscriptionServiceExist,
	} = useContext(SubscriptionContext);

	const loadTokens = useRef(false);

	const [ pageMode,                        setPageMode                        ] = useState<'simple' | 'advanced'>('simple');
	const [ wrapWith,                        setWrapWith                        ] = useState<'own' | 'predefined' | 'empty'>('empty');
	const [ currentPage,                     setCurrentPage                     ] = useState<'saft' | 'addcoll' | 'mint'>('saft');

	const [ batchWorkerContract,             setBatchWorkerContract             ] = useState<string>('');
	const [ wrapperContract,                 setWrapperContract                 ] = useState<string>('');
	const [ defaultStorages,                 setDefaultStorages                 ] = useState(Array<{ contractAddress: string, tokenId: string, assetType: _AssetType }>);
	const [ techToken,                       setTechToken                       ] = useState('');
	const [ nftMinterContract,               setNftMinterContract               ] = useState<string>('');
	const [ storageContracts,                setStorageContracts                ] = useState<Array<string>>([]);
	const [ whitelistContract,               setWhitelistContract               ] = useState<string | undefined>(undefined);
	const [ maxCollaterals,                  setMaxCollaterals                  ] = useState<number>(25);

	const [ originalTokensBlacklist,         setOriginalTokensBlacklist         ] = useState<Array<string> | undefined>(undefined);
	const [ enabledForFee,                   setEnabledForFee                   ] = useState<Array<string>>([]);
	const [ enabledForCollateral,            setEnabledForCollateral            ] = useState<Array<string>>([]);
	const [ enabledRemoveFromCollateral,     setEnabledRemoveFromCollateral     ] = useState<Array<string>>([]);

	const [ inputOriginalTokenAddress,       setInputOriginalTokenAddress       ] = useState('');
	const [ savedAddresses,                  setSavedAddresses                  ] = useState<Array<{ address: string, standart: _AssetType, chainId: number }>>([]);
	const [ predefinedContracts,             setPredefinedContracts             ] = useState<Array<{ address: string, standart: _AssetType }>>([]);
	const [ inputTimeLockDays,               setInputTimeLockDays               ] = useState('');

	const [ requestOriginalTokenAddress,     setRequestOriginalTokenAddress     ] = useState<string | undefined>(undefined);
	const [ fetchedTokens,                   setFetchedTokens                   ] = useState<Array<FetchedTokensOfContractType>>([]);

	const [ inputOutAssetType,               setInputOutAssetType               ] = useState<_AssetType>(_AssetType.ERC721);
	const [ inputCopies,                     setInputCopies                     ] = useState(2);

	const [ advancedOptionsOpened,           setAdvancedOptionsOpened           ] = useState(false);
	const [ inputRules,                      setInputRules                      ] = useState<Rules>(decodeRules('0000'));
	const [ collateralPopupShown,            setCollateralPopupShown            ] = useState<boolean>(false);

	const [ inputTransferFeeAddress,         setInputTransferFeeAddress         ] = useState('');
	const [ inputTransferFeeAmount,          setInputTransferFeeAmount          ] = useState('');

	const [ inputRoyaltyRecipientAddress,    setInputRoyaltyRecipientAddress    ] = useState('');
	const [ royaltyRecipients,               setRoyaltyRecipients               ] = useState<Array<RoyaltyInput>>([]);
	const [ inputAddWNFTChecked,             setInputAddWNFTChecked             ] = useState(false);

	const [ collaterals,                     setCollaterals                     ] = useState<Array<CollateralItem>>([]);
	const [ inputCollateralAddress,          setInputCollateralAddress          ] = useState('');
	const [ inputCollateralAmount,           setInputCollateralAmount           ] = useState('');

	const [ inputWNFTRecipientAddress,       setInputWNFTRecipientAddress       ] = useState('');
	const [ inputWNFTRecipientTokenId,       setInputWNFTRecipientTokenId       ] = useState('');
	const [ WNFTRecipients,                  setWNFTRecipients                  ] = useState<Array<SAFTRecipientItem>>([]);

	const [ showErrors,                      setShowErrors                      ] = useState<Array<string>>([]);

	const [ copiedLabels,                    setCopiedLabels                    ] = useState<Array<string>>([]);

	const [ showCreateCustomContractModal,   setShowCreateCustomContractModal   ] = useState(false);
	const [ userContractName,                setUserContractName                ] = useState<string>('');
	const [ userContractSymbol,              setUserContractSymbol              ] = useState<string>('');

	const [ batchWorkerCollectionsContract,  setBatchWorkerCollectionsContract  ] = useState<undefined | string>(undefined);
	const [ userWrapper,                     setUserWrapper                     ] = useState<undefined | string>(undefined);
	const [ userCollectionsList,             setUserCollectionsList             ] = useState<Array<{ contractAddress: string, assetType: _AssetType, name?: string }>>([]);
	const [ userCollectionsListCopied,       setUserCollectionsListCopied       ] = useState<string | undefined>(undefined);
	const [ userStorageToWrap,               setUserStorageToWrap               ] = useState<undefined | string>();

	const location = useLocation();

	// fill original contract address from url
	useEffect(() => {
		const sourceParams = [
			{ path: "/:chainId/:contractAddress/:tokenId" },
			{ path: "/:chainId/:contractAddress" },
			{ path: "/:chainId" },
		];
		const matches = matchRoutes(sourceParams, location);

		if ( matches && matches[0] && matches[0].params && matches[0].params.chainId && parseInt(matches[0].params.chainId) ) {
			if ( matches[0].params.contractAddress ) {
				setPageMode('advanced');
				setWrapWith('own');
				setInputOriginalTokenAddress(matches[0].params.contractAddress);
			}
		}

	}, [ location ]);

	// get params from localstorage
	useEffect(() => {
		if ( !currentChain ) { return; }

		let _savedAddresses = [];
		const addressesFromLocalStorage = localStorageGet('SAFT_originalContracts');
		if ( addressesFromLocalStorage ) {
			try {
				_savedAddresses = JSON.parse(addressesFromLocalStorage).filter((item: { address: string, standart: _AssetType, chainId: number }) => {
					return item.chainId === currentChain.chainId
				});
				setSavedAddresses(_savedAddresses)
			}catch(e) {
				localStorageRemove('SAFT_originalContracts');
			}
		}
		const _predefinedContracts = [];
		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChain ) {
			setError('Unsupported chain');
			return;
		}
		const minter721 = foundChain.nftMinterContract721;
		if ( minter721 ) {
			_predefinedContracts.push({
				address: minter721,
				standart: _AssetType.ERC721,
			});
		}
		const minter1155 = foundChain.nftMinterContract1155;
		if ( minter1155 ) {
			_predefinedContracts.push({
				address: minter1155,
				standart: _AssetType.ERC1155,
			});
		}
		setPredefinedContracts(_predefinedContracts)

	}, [ currentChain ]);

	const getBatchWorkerCollectionsParams = async () => {

		if ( !currentChain ) { return; }
		if ( !userAddress ) { return; }

		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChain || !foundChain.batchWorker ) {
			setError('Unsupported chain');
			setBatchWorkerContract('');
			loadTokens.current = false;
			return;
		}

		const batchWorkerCollections = foundChain.userBatchWorkerCollections;
		setBatchWorkerCollectionsContract(batchWorkerCollections);
		if ( !batchWorkerCollections ) {
			setUserCollectionsList([]);
			return;
		}

		const userWrapper = foundChain.userBatchWorkerWrapper;
		setUserWrapper(userWrapper);

		const userBatchWrappers = await getUserBatchWrappers(currentChain.chainId, batchWorkerCollections, userAddress);
		const userBatchWrappersWithNames = [];
		for (let idx = 0; idx < userBatchWrappers.length; idx++) {
			const item = userBatchWrappers[idx];

			try {
				const name = await getContractNameERC721(currentChain.chainId, item.contractAddress);
				userBatchWrappersWithNames.push({ ...item, name });
			} catch(e) {
				console.log('Cannot fetch name of contract', item.contractAddress);
			}

			// delay for limit rps to node
			await new Promise((resolve) => {
				setTimeout( resolve, 500 );
			});
		}
		setUserCollectionsList(userBatchWrappersWithNames);

	}
	useEffect(() => {

		getBatchWorkerCollectionsParams();

	}, [ currentChain, userAddress ]);

	// fill wrapper params
	useEffect(() => {
		const getWrapperParams = async () => {

			if ( !currentChain ) { return; }

			if ( loadTokens.current ) { return; }
			loadTokens.current = true;

			const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
			if ( !foundChain || !foundChain.batchWorker ) {
				setError('Unsupported chain');
				setBatchWorkerContract('');
				loadTokens.current = false;
				return;
			}

			const _batchWorker = foundChain.batchWorker;
			setBatchWorkerContract(_batchWorker);
			console.log('_batchWorker', _batchWorker);

			const _wrapperContract = await getWrapperOfBatchWorker(currentChain.chainId, _batchWorker);
			if ( !_wrapperContract ) {
				loadTokens.current = false;
				return;
			}
			setWrapperContract(_wrapperContract);
			console.log('_wrapperContract', _wrapperContract);

			const storages = [];
			const storage721 = await getWrapperLastMinted(currentChain.chainId, _wrapperContract, _AssetType.ERC721);
			if ( storage721.contractAddress !== '0x0000000000000000000000000000000000000000' ) {
				storages.push({ ...storage721, assetType: _AssetType.ERC721 });
			}
			const storage1155 = await getWrapperLastMinted(currentChain.chainId, _wrapperContract, _AssetType.ERC1155);
			if ( storage1155.contractAddress !== '0x0000000000000000000000000000000000000000' ) {
				storages.push({ ...storage1155, assetType: _AssetType.ERC721 });
			}
			setDefaultStorages(storages);

			const _techToken = await getWrapperTechToken(currentChain.chainId, _wrapperContract);
			setTechToken(_techToken);

			const _storageContracts = foundChain.WNFTStorageContracts.map((item) => { return item.contractAddress });
			setStorageContracts(_storageContracts);

			const _maxCollaterals = await getMaxCollaterals(currentChain.chainId, _wrapperContract);
			setMaxCollaterals(_maxCollaterals);

			const _whitelistContract = await getWrapperWhitelist(currentChain.chainId, _wrapperContract);
			if ( !_whitelistContract ) {
				setWhitelistContract(undefined);
				setEnabledForCollateral([]);
				setEnabledForFee([]);
				setEnabledRemoveFromCollateral([]);
				setOriginalTokensBlacklist([]);
				return;
			}

			const _whitelist = await getWhitelist(currentChain.chainId, _whitelistContract);
			await Promise.all(_whitelist.map(async (item) => {
				if ( item.assetType === _AssetType.ERC20 ) {
					requestERC20Token(item.contractAddress, userAddress);
				}
				return item.contractAddress;
			}));
			for (const idx in _whitelist) {
				if (Object.prototype.hasOwnProperty.call(_whitelist, idx)) {
					const item = _whitelist[idx];

					if ( item.assetType !== _AssetType.ERC20 ) { return; }

					// delay for limit rps to node
					await new Promise((resolve) => {
						setTimeout( resolve, 500 );
					});

					if ( await isEnabledForCollateral(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledForCollateral((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
					if ( await isEnabledForFee(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledForFee((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
					if ( await isEnabledRemoveFromCollateral(currentChain.chainId, _whitelistContract, item.contractAddress) ) {
						setEnabledRemoveFromCollateral((prevState) => {
							return [
								...prevState.filter((iitem) => { return iitem.toLowerCase() !== item.contractAddress.toLowerCase() }),
								item.contractAddress,
							]
						});
					}
				}
			}

			if ( _whitelistContract ) {

				const _blacklist = await getBlacklist(currentChain.chainId, _whitelistContract);
				if ( _blacklist ) {
					const _blacklistParsed = await Promise.all(_blacklist.map(async (item) => {
						if ( item.assetType === _AssetType.ERC20 ) {
							requestERC20Token(item.contractAddress, userAddress);
						}
						return item.contractAddress;
					}));
					setOriginalTokensBlacklist(_blacklistParsed);
				}

				let idx = 0;
				while ( true ) {
					try {
						const _whitelistItem = await getWhitelistItem(currentChain.chainId, _whitelistContract, idx);

						if ( decodeAssetTypeFromIndex(`${_whitelistItem.assetType}`) === _AssetType.ERC20 ) {
							requestERC20Token(_whitelistItem.contractAddress, userAddress);
						}

						if ( await isEnabledForCollateral(currentChain.chainId, _whitelistContract, _whitelistItem.contractAddress) ) {
							setEnabledForCollateral((prevState) => {
								return [
									...prevState.filter((item) => { return item.toLowerCase() !== _whitelistItem.contractAddress.toLowerCase() }),
									_whitelistItem.contractAddress,
								]
							});
						}
						if ( await isEnabledForFee(currentChain.chainId, _whitelistContract, _whitelistItem.contractAddress) ) {
							setEnabledForFee((prevState) => {
								return [
									...prevState.filter((item) => { return item.toLowerCase() !== _whitelistItem.contractAddress.toLowerCase() }),
									_whitelistItem.contractAddress,
								]
							});
						}
						// if ( await isEnabledRemoveFromCollateral(currentChain.chainId, _whitelistContract, _whitelistItem.contractAddress) ) {
						// 	setEnabledRemoveFromCollateral((prevState) => {
						// 		return [
						// 			...prevState.filter((item) => { return item.toLowerCase() !== _whitelistItem.contractAddress.toLowerCase() }),
						// 			_whitelistItem.contractAddress,
						// 		]
						// 	});
						// }
					} catch(e) { break; }

					// await new Promise((resolve) => {
					// 	setTimeout( resolve, 500 );
					// });

					idx += 1;
				}

			}

			loadTokens.current = false;
		}

		getWrapperParams();

	}, [ currentChain ]);

	// fetch tokens
	useEffect(() => {

		const fetchTokens = async (address: string, name: string, page: number) => {

			const tokensOnPage = 12;

			if ( !currentChain ) { return; }
			if ( !userAddress ) { return; }

			// no chain fallback
			getUserNFTsOfContractFromAPI(currentChain.chainId, _AssetType.ERC721, userAddress, address, page)
				.then((data) => {
					if ( data.length === tokensOnPage ) {
						fetchTokens(address, name, page + 1);
					}
					setFetchedTokens((prevState) => {
						const foundContract = prevState.find((item) => { return item.contractAddress.toLowerCase() === address.toLowerCase() });
						const tokensFiltered = (foundContract?.tokens || []).filter((item) => {
							return !data.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() && iitem.tokenId.toLowerCase() === item.tokenId.toLowerCase() })
						})
						return [
							...prevState.filter((item) => { return item.contractAddress.toLowerCase() !== address.toLowerCase() }),
							{
								contractAddress: address,
								isLoading: data.length === tokensOnPage,
								collectionName: name,
								tokens: [
									...tokensFiltered,
									...data
								],
								badContract: false,
							}
						]
					})
				})
				.catch((e: any) => {
					console.log(`Cannot fetch tokens of contract: ${ address }`, e);
				})
		}

		if ( !currentChain ) { return; }
		if ( !requestOriginalTokenAddress ) { return; }
		if ( !isAddress(requestOriginalTokenAddress) ) { return; }

		if ( originalTokensBlacklist ) {
			const foundBlacklistItem = originalTokensBlacklist.find((item) => {
				return item.toLowerCase() === requestOriginalTokenAddress.toLowerCase()
			});
			if ( foundBlacklistItem ) { return; }
		}

		const alreadyFetched = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === requestOriginalTokenAddress.toLowerCase() });
		if ( alreadyFetched ) { return; }

		getContractNameERC721(currentChain?.chainId, requestOriginalTokenAddress)
			.then((data) => { fetchTokens(requestOriginalTokenAddress, data, 1); })
			.catch((e) => {
				console.log(`Cannot fetch name of contract: ${ requestOriginalTokenAddress }`, e);
				setFetchedTokens((prevState) => {
					return [
						...prevState.filter((item) => { return item.contractAddress.toLowerCase() !== requestOriginalTokenAddress.toLowerCase() }),
						{
							contractAddress: requestOriginalTokenAddress,
							isLoading: false,
							collectionName: '',
							tokens: [],
							badContract: true,
						}
					]
				})
			})

	}, [ requestOriginalTokenAddress ]);

	const filterERC20ContractByPermissions = (permissions: {
		enabledForCollateral?        : boolean,
		enabledForFee?               : boolean,
		enabledRemoveFromCollateral? : boolean,
	}): Array<ERC20Type> => {
		if ( whitelistContract === undefined ) {
			if ( currentChain ) {
				return [
					chainTypeToERC20(currentChain),
					...erc20List
				];
			} else {
				return erc20List;
			}
		}

		// OR LOGIC
		const filteredERC20 = erc20List.filter((item) => {
			if ( permissions.enabledForCollateral        && enabledForCollateral.find(       (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledForFee               && enabledForFee.find(              (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledRemoveFromCollateral && enabledRemoveFromCollateral.find((iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }

			return false;
		})
		if ( !currentChain ) { return filteredERC20; }

		return [
			chainTypeToERC20(currentChain),
			...filteredERC20
		]
	}
	const addressIsStorage = (address: string): boolean => {
		return !!storageContracts.find((item => { return item.toLowerCase() === address.toLowerCase() }))
	}

	const getPageSelector = () => {
		return (
			<div className="col-12 col-sm-auto order-md-1">
				<div className="db-section__toggle">
					<button
						className={`tab ${ currentPage === 'saft' ? 'active' : '' }`}
						onClick={() => { setCurrentPage('saft') }}
					>SAFT</button>
					<button
						className={`tab ${ currentPage === 'addcoll' ? 'active' : '' }`}
						onClick={() => { setCurrentPage('addcoll') }}
					>Add Collateral</button>
				</div>
			</div>
		)
	}

	const getModeSelector = () => {
		return (
			<div className="col-12 col-md-auto mb-3">
				<div className="wf-settings__switcher">
					<div className="label d-sm-none">Mode</div>
					<div className="switcher">
						<input
							className="toggle toggle-left"
							type="radio"
							id="simple-on"
							checked={ pageMode === 'simple' }
							onChange={() => {
								setPageMode('simple');
							}}
						/>
						<label
							className="switcher__btn"
							htmlFor="simple-on"
						>
							Simple
						</label>
						<input
							className="toggle toggle-right"
							id="advanced-on"
							value="true"
							type="radio"
							checked={ pageMode === 'advanced' }
							onChange={() => {
								setPageMode('advanced');
							}}
						/>
						<label
							className="switcher__btn"
							htmlFor="advanced-on"
						>Advanced</label>
					</div>
				</div>
			</div>
		)
	}

	const subscriptionBlocked = () => {

		if ( batchWorkerContract === '' ) { return true; }

		if ( !subscriptionServiceExist ) { return false; }

		if ( !subscriptionRemainings ) { return true; }
		if ( subscriptionRemainings.countsLeft.gt(0) ) { return false; }

		const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
		if ( subscriptionRemainings.validUntil.gt(now) ) {
			return false;
		}
	}
	const getSubscriptionBlock = () => {
		return (
			<div className="col-12 col-sm order-sm-2 mb-4 mb-sm-0">
				<SubscriptionRenderer
					className="text-sm-right mb-0"
					btnClassName="btn-md"
				/>
			</div>
		)
	}

	const deployUserContractSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		if ( !batchWorkerCollectionsContract ) { return; }

		setLoading('Waiting to deploy');

		let txResp;
		try {
			if ( isMultisig ) {
				txResp = await deployUserBatchWorkerMultisig(
					_web3,
					batchWorkerCollectionsContract,
					{
						name: userContractName,
						symbol: userContractSymbol,
					}
				);
			} else {
				txResp = await deployUserBatchWorker(
					_web3,
					batchWorkerCollectionsContract,
					{
						name: userContractName,
						symbol: userContractSymbol,
					}
				);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`Collection contract ${batchWorkerCollectionsContract}`,
					`User address: ${_userAddress}`,
					`Args: ${JSON.stringify({
						name: userContractName,
						symbol: userContractSymbol,
					})}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		unsetModal();

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setUserContractName('');
						setUserContractSymbol('');
						getBatchWorkerCollectionsParams();
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Tokens successfully wrapped`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setUserContractName('');
						setUserContractSymbol('');
						getBatchWorkerCollectionsParams();
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}
	const getCustomContractModal = () => {
		if ( !showCreateCustomContractModal ) { return null; }

		return (
			<div className="modal">
				<div
					className="modal__inner"
					onClick={(e) => {
						e.stopPropagation();
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setShowCreateCustomContractModal(false);
						}
					}}
				>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => { setShowCreateCustomContractModal(false) }}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="c-add">
								<div className="c-add__text">
									<div className="h2">Create your own wNFT collection</div>
								</div>
								<div className="c-add__form">
									<div className="row mb-3">
										<div className="col-12">
											<span className="input-label">
												Choose a name and a symbol for your personal Token Collection
											</span>
										</div>
									</div>
									<div className="row mb-6">
										<div className="col-12 col-lg-6">
											<input
												className="input-control"
												placeholder="Token Collection Name"
												value={userContractName}
												onChange={(e) => {
													setUserContractName(e.target.value);
												}}
											/>
										</div>
										<div className="col-12 col-lg-6">
											<input
												className="input-control"
												placeholder="Token Collection Symbol"
												value={userContractSymbol}
												onChange={(e) => {
													setUserContractSymbol(e.target.value);
												}}
											/>
										</div>
									</div>
									<div className="row">
										<div className="col-sm-3 mb-2 mb-sm-0"></div>
										<div className="col-sm-6">
											<div className="c-wrap mb-0">
												<button
													className="btn btn-grad w-100"
													onClick={async () => {
														if ( !currentChain ) { return; }
														let _web3 = web3;
														let _userAddress = userAddress;
														let _currentChain = currentChain;

														if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
															setLoading('Waiting for wallet');

															try {
																const web3Params = await getWeb3Force(_currentChain.chainId);

																_web3 = web3Params.web3;
																_userAddress = web3Params.userAddress;
																_currentChain = web3Params.chain;
																unsetModal();
															} catch(e: any) {
																setModal({
																	type: _ModalTypes.error,
																	title: 'Error with wallet',
																	details: [
																		e.message || e
																	]
																});
																return;
															}
														}

														if ( !_web3 || !_userAddress || !_currentChain ) { return; }
														const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
														if ( isMultisig && wrapWith === 'predefined' ) {
															setInfo('Predefined contracts are not available for multisig wallets yet');
															return;
														}
														deployUserContractSubmit(_currentChain, _web3, _userAddress, isMultisig);
													}}
												>Create collection</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getCustomContractRow = (item: { contractAddress: string, assetType: _AssetType, name?: string }) => {
		return (
			<div className="mb-3" key={ item.contractAddress }>
				<label className="checkbox">
					<input
						type="radio"
						name="my-contracts"
						checked={ userStorageToWrap === item.contractAddress }
						onChange={(e) => {
							setUserStorageToWrap(item.contractAddress)
						}}
					/>
					<span className="check"> </span>
					<span className="check-text ">{ item.name ? `${item.name} — ` : '' }{ compactString(item.contractAddress) }</span>
					<CopyToClipboard
						text={ item.contractAddress }
						onCopy={() => {
							setUserCollectionsListCopied(item.contractAddress);
							setTimeout(() => {
								setUserCollectionsListCopied(undefined);
							}, 5*1000);
						}}
					>
						<button className="btn-copy">
							&nbsp;
							<img src={ i_copy } alt="" />
							{
								userCollectionsListCopied === item.contractAddress ? (
									<span className="btn-action-info">Copied</span>
								) : null
							}
						</button>
					</CopyToClipboard>
				</label>
			</div>
		)
	}

	const getContractsBlock = () => {

		if ( pageMode !== 'advanced' ) { return null; }
		if ( !batchWorkerCollectionsContract ) { return null; }

		let storageAddress = '';
		if ( defaultStorages.length ) {
			const foundStorage = defaultStorages.find((item) => { return item.assetType === _AssetType.ERC721 });
			if ( foundStorage ) {
				storageAddress = foundStorage.contractAddress;
			} else {
				storageAddress = defaultStorages[0].contractAddress;
			}
		}

		return (
			<div className="row">
				<div className="col col-12">
					<div className="c-wrap">
						<div className="col-12">
							<p>Available ERC-721 contracts list:</p>
							<div className="my-contracts">
								<div className="mb-3">
									<label className="checkbox">
										<input
											type="radio"
											name="my-contracts"
											checked={ userStorageToWrap === undefined }
											onChange={(e) => {
												setUserStorageToWrap(undefined)
											}}
										/>
										<span className="check"> </span>
										<span className="check-text check-text-bold"><b>Public wNFT Collection</b></span>
										<CopyToClipboard
											text={ batchWorkerContract }
											onCopy={() => {
												setUserCollectionsListCopied('default');
												setTimeout(() => {
													setUserCollectionsListCopied(undefined);
												}, 5*1000);
											}}
										>
											<button className="btn-copy">
												&nbsp;
												<img src={ i_copy } alt="" />
												{
													userCollectionsListCopied === 'default' ? (
														<span className="btn-action-info">Copied</span>
													) : null
												}
											</button>
										</CopyToClipboard>
									</label>
								</div>

								{ userCollectionsList.map((item) => { return getCustomContractRow(item) }) }

								<div className="d-inline-block mr-2 my-3">
									<button
										className="btn btn-outline"
										onClick={() => { setShowCreateCustomContractModal(true); }}
									>Create own wNFT collection</button>
								</div>
							</div>
						</div>
					</div>
				<div className="col-md-5 col-lg-5"></div>
				</div>
			</div>
		)
	}

	const getPredefinedContractTip = () => {
		if ( !predefinedContracts.length ) {
			return (
				<TippyWrapper
					msg="No minter contract in current chain"
				></TippyWrapper>
			)
		}
		if ( localStorageGet('authMethod').toLowerCase() === 'safe' ) {
			return (
				<TippyWrapper
					msg="Predefined contracts are not available for multisig wallets yet"
				></TippyWrapper>
			)
		}

		return (
			<TippyWrapper
				msg="Create wNFT with original NFT inside using Envelop metadata"
			></TippyWrapper>
		)
	}
	const getWrapWithBlock = () => {
		if ( pageMode === 'simple' ) { return null; }

		return (
			<div className="row row-sm mb-5">
				<div className="col-12 col-lg-auto pt-2 pb-3 pb-lg-0">
					<label className="input-label mb-0">I want to:</label>
				</div>
				<div className="col-auto py-2">
					<label className="checkbox">
						<input
							type="radio"
							name="wrap-with"
							disabled={ subscriptionBlocked() }
							checked={ wrapWith === 'own' }
							onChange={() => {
								if ( wrapWith === 'predefined' ) { setInputOriginalTokenAddress(''); }
								setWrapWith('own');
							}}
						/>
						<span className="check"> </span>
						<span className="check-text">
							use/mint my nft contract
							<TippyWrapper
								msg="Create wNFT with original custom NFT inside"
							></TippyWrapper>
						</span>
					</label>
				</div>
				<div className="col-auto py-2">
					<label className="checkbox">
						<input
							type="radio"
							name="wrap-with"
							disabled={ subscriptionBlocked() || !predefinedContracts.length }
							checked={ wrapWith === 'predefined' }
							onChange={() => {
								if ( wrapWith === 'own' ) { setInputOriginalTokenAddress(''); }
								setWrapWith('predefined');
							}}
						/>
						<span className="check"> </span>
						<span className="check-text">
							mint tokens with ENVELOP contract
							{ getPredefinedContractTip() }
						</span>
					</label>
				</div>
				<div className="col-auto py-2">
					<label className="checkbox">
						<input
							type="radio"
							name="wrap-with"
							disabled={ subscriptionBlocked() }
							checked={ wrapWith === 'empty' }
							onChange={() => {
								setWrapWith('empty');
								// setInputOriginalTokenAddress('');
							}}
						/>
						<span className="check"> </span>
						<span className="check-text">
							wrap emptiness
							<TippyWrapper
								msg="Create wNFT without original NFT inside"
							></TippyWrapper>
						</span>
					</label>
				</div>
			</div>
		)
	}

	const getTokensQty = () => {
		if ( !currentChain ) { return null; }

		const foundContract = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase() });
		if ( !foundContract ) { return null; }

		if ( foundContract.isLoading ) {
			return (
				<div className="collection-name">
					<b>Loading</b>
				</div>
			)
		}
		if ( foundContract.tokens.length ) {
			return (
				<>
				<div className="collection-name">
					<b>
						<a
							className="ex-link"
							target="_blank"
							rel="noopener noreferrer"
							href={ combineURLs(currentChain.explorerBaseUrl, `/address/${inputOriginalTokenAddress}`) }
						>
							{ foundContract.collectionName }
							<img className="i-ex" src={ icon_external } alt="" />
						</a>
					</b>
				</div>
				<div className="collection-name">
					<span>You have:&nbsp;</span>
					<b>{ foundContract.tokens.length } { foundContract.tokens.length === 1 ? 'token' : 'tokens' }</b>
				</div>
				</>
			)
		}
	}
	const getOriginalContractInput = () => {
		if ( wrapWith === 'empty' ) { return null; }

		let inputOptions: Array<{ value: string, label: string }> = [];
		if ( wrapWith === 'predefined' ) {
			inputOptions = predefinedContracts.map((item: { address: string, standart: _AssetType }) => {
				return { value: item.address, label: `${assetTypeToString(item.standart, currentChain?.EIPPrefix || 'ERC')} — ${compactString(item.address, 10)}` }
			})
		} else {
			inputOptions = savedAddresses.map((item: string | { address: string, standart: _AssetType, chainId: number }) => {
				if ( typeof item === 'string' ) {
					return { value: item, label: `${compactString(item, 10)}` }
				}
				return { value: item.address, label: `${assetTypeToString(item.standart, currentChain?.EIPPrefix || 'ERC')} — ${compactString(item.address, 10)}` }
			})
		}

		return (
			<div className="col col-12 col-md-8 col-lg-6">
				<label className="input-label">Original NFT Address</label>
				<InputWithOptions
					placeholder="Paste here"
					value={ inputOriginalTokenAddress }
					onChange={(e) => {
						const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
						setInputOriginalTokenAddress(value);
						setRequestOriginalTokenAddress(value);
					}}
					onSelect={(e) => {
						const value = e.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
						setInputOriginalTokenAddress(value);
						setRequestOriginalTokenAddress(value);
					}}
					options={ inputOptions }
					showArrow={ false }
				/>
				{/* { getCollectionBlock() } */}
				{ getTokensQty() }
			</div>
		)
	}
	const getDaysPretty = () => {
		const daysToAdd = parseInt(inputTimeLockDays) || 0;
		if ( daysToAdd === 0 ) { return null; }

		return (
			<div className="collection-name">
				<span>Date&nbsp;</span>
				<b>{ dateToStrWithMonth(addDays(daysToAdd)) }</b>
			</div>
		)
	}
	const getPredefinedContractWarnings = () => {
		if ( wrapWith !== 'predefined' ) { return null; }
		if ( inputOriginalTokenAddress === '' ) { return null; }
		if ( !Web3.utils.isAddress(inputOriginalTokenAddress) ) { return null; }

		const foundContract = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase() })
		if ( !foundContract ) { return null; }

		if ( foundContract.tokens.length === 0 ) {
			return (
				<div className="row mt-2">
					<div className="col-12 col-sm-7 col-md-auto mb-2 mb-sm-0">
						<div className="text-warning my-2">
							You have no NFTs in&nbsp;this contract. <br/ >
							You&nbsp;should mint at&nbsp;least one token to&nbsp;proceed.
						</div>
					</div>
					<div className="col-12 col-sm-4 col-md-auto">
						<button
							className="btn btn-outline d-inline-flex w-100"
							onClick={() => {
								setCurrentPage('mint');
								// window.location.href = '/mintnew';
							}}
						>Mint a&nbsp;NFT</button>
					</div>
				</div>
			)
		}

		return null;
	}
	const getOwnContractWarnings = () => {
		if ( wrapWith !== 'own' ) { return null; }
		if ( inputOriginalTokenAddress !== '' && Web3.utils.isAddress(inputOriginalTokenAddress) ) {

			if ( originalTokensBlacklist ) {
				const foundBlacklistItem = originalTokensBlacklist.find((item) => {
					return item.toLowerCase() === inputOriginalTokenAddress.toLowerCase()
				});
				if ( foundBlacklistItem ) {
					return (
						<div className="row mt-2">
							<div className="col-12 col-sm-7 col-md-auto mb-2 mb-sm-0">
								<div className="text-warning my-2">
									Token address is blacklisted
								</div>
							</div>
						</div>
					)
				}
			}

			const foundContract = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase() })
			if ( !foundContract ) { return null; }

			if ( foundContract.badContract ) {
				return (
					<div className="row mt-2">
						<div className="col-12 col-sm-12 mb-2 mb-sm-0">
							<div className="text-warning my-2">
								Cannot load data from contract
							</div>
						</div>
					</div>
				)
			}

			if ( foundContract.tokens.length === 0 ) {
				return (
					<div className="row mt-2">
					<div className="col-12 col-sm-12 mb-2 mb-sm-0">
						<div className="text-warning my-2">
							You have no NFTs in&nbsp;this contract. <br/ >
							You&nbsp;should mint at&nbsp;least one token to&nbsp;proceed.
						</div>
					</div>
					<div className="col-12 col-sm-12 mb-2 mb-sm-0">
						<p className="text-orange">You can only  wrap the original ERC-721 tokens</p>
					</div>
					</div>
				)
			}
		}

		return (
			<div className="row">
				<div className="col-12 col-sm-12 mb-2 mb-sm-0">
					<p className="text-orange">You can only  wrap the original ERC-721 tokens</p>
				</div>
			</div>
		)

	}

	const getOriginalContractBlock = () => {

		return (
			<div
				className="c-wrap c-b-wrap"
				ref={ scrollable.originalToken }
			>
				{ getWrapWithBlock() }
				<div className="c-wrap__form">
					<div className="row">
						{ getOriginalContractInput() }
						<div
							className="col col-12 col-md-4 col-lg-3"
							ref={ scrollable.timelock }
						>
							<label className="input-label">Unlock Time <small>(days)</small></label>
							<input
								className="input-control"
								type="text"
								placeholder=""
								value={ inputTimeLockDays }
								disabled={ subscriptionBlocked() || inputRules.noUnwrap }
								onChange={(e) => {
									const value = e.target.value.replaceAll(' ', '');
									if (
										value === ''
									) {
										setInputTimeLockDays('');
										return;
									}
									if (
										isNaN(parseInt(value)) ||
										parseInt(value) > 9999999
									) {
										return;
									}

									setInputTimeLockDays(`${parseInt(value)}`);
								}}
							/>
							{ getDaysPretty() }
						</div>

					</div>

					{ getPredefinedContractWarnings() }
					{ getOwnContractWarnings() }

				</div>
			</div>
		)
	}

	const getCopiesBlock = () => {
		const showError = showErrors.find((item) => { return item === 'standart' });

		if ( inputOutAssetType === _AssetType.ERC1155 ) {
			return (
				<div className="col-12 col-md-2">
					<div className="input-group mb-md-0">
						<label className="input-label">Copies</label>
						<NumberInput
							value={ inputCopies }
							onChange={(e: number | undefined) => {
									setInputCopies(e || 1);
									setShowErrors( showErrors.filter((item) => { return item !== 'standart' }) );
							}}
							min={ 1 }
							inputClass={ showError ? 'has-error' : '' }
						/>
					</div>
				</div>
			)
		}
	}
	const getStandartSelectorBlock = () =>  {

		if ( pageMode === 'simple' ) { return null; }
		if ( userStorageToWrap !== undefined ) { return null; }

		return (
			<div
				className="c-wrap"
				ref={ scrollable.standart }
			>
				<div className="c-wrap__header">
					<div className="h3">wNFT Standard</div>
				</div>
				<div className="row">
					<div className="col-12 col-md-5">
						<div className="input-group mb-md-0">
							<label className="input-label">I want to create:</label>
							<div className="row row-sm">
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="wnft-standard"
											value={ _AssetType.ERC721 }
											disabled={ subscriptionBlocked() }
											checked={ inputOutAssetType === _AssetType.ERC721 }
											onChange={() => { setInputOutAssetType(_AssetType.ERC721) }}
										/>
										<span className="check"> </span>
										<span className="check-text"><b>{ currentChain?.EIPPrefix || 'ERC' }-721</b></span>
									</label>
								</div>
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="wnft-standard"
											value={ _AssetType.ERC1155 }
											disabled={ subscriptionBlocked() }
											checked={ inputOutAssetType === _AssetType.ERC1155 }
											onChange={() => { setInputOutAssetType(_AssetType.ERC1155) }}
										/>
										<span className="check"> </span>
										<span className="check-text"> <b>{ currentChain?.EIPPrefix || 'ERC' }-1155</b></span>
									</label>
								</div>
							</div>
						</div>
					</div>
					{ getCopiesBlock() }
				</div>
			</div>
		)
	}

	const getAdvancedOptionsBlock = () =>  {
		if ( pageMode === 'simple' ) { return null; }

		return (
			<div
				className="c-wrap p-0"
				ref={ scrollable.advancedOptions }
			>
				<div
					className={`c-wrap__toggle ${advancedOptionsOpened ? 'active' : ''}`}
					onClick={() => { setAdvancedOptionsOpened(!advancedOptionsOpened); }}
				>
					<div className="h3 mb-0"><b>Advanced options</b></div>
				</div>

				<div className="c-wrap__dropdown">
					<div className="row">
						<div className="col-12">
							<div className="input-group mb-0">
								<label className="input-label pb-1">Disable:</label>
								<div className="mb-3">
									<label className="checkbox">
										<input
											type="checkbox"
											name=""
											checked={ inputRules.noUnwrap }
											onChange={(e) => {
												setInputRules({
													...inputRules,
													noUnwrap: e.target.checked
												});
												setInputTimeLockDays('');

												if ( !collateralPopupShown ) {
													if ( collaterals.length ) {
														setModal({
															type: _ModalTypes.info,
															title: 'Warning',
															text: [{ text: 'You can lock collateral forever with these settings', clazz: 'text-orange' }]
														});
														setCollateralPopupShown(true);
													}
												}
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">
											Unwrapping
											<TippyWrapper
												msg="The owner of a future wNFT will be unable to unwrap it"
												elClass="ml-1"
											></TippyWrapper>
										</span>
									</label>
								</div>
								<div className="mb-3">
									<label className="checkbox">
										<input
											type="checkbox"
											name=""
											checked={ inputRules.noAddCollateral }
											onChange={(e) => {
												setInputRules({
													...inputRules,
													noAddCollateral: e.target.checked
												});
												setInputAddWNFTChecked(e.target.checked);
												setTimeout(() => { removeRoyaltyRecipient(undefined) }, 1);
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">
											Adding collateral
											<TippyWrapper
												msg="No one will be able to add collateral to future wNFT transactions"
												elClass="ml-1"
											></TippyWrapper>
										</span>
									</label>
								</div>
								<div>
									<label className="checkbox">
										<input
											type="checkbox"
											name=""
											checked={ inputRules.noTransfer }
											onChange={(e) => {
												if ( e.target.checked ) {
													setInputRules({
														...inputRules,
														noTransfer: e.target.checked,
													});
													setCollaterals( collaterals.filter((item: CollateralItem) => { return !item.amount || !item.amount.eq(0) }) );
												} else {
													if ( inputAddWNFTChecked && inputTransferFeeAmount !== '' ) {
														setTimeout(() => { addCollateralRow(inputTransferFeeAddress, inputTransferFeeAmount) }, 1);
													}
													setInputRules({
														...inputRules,
														noTransfer: e.target.checked
													});
												}
											}}
										/>
										<span className="check"> </span>
										<span className="check-text">
											Transferring (Soulbound <span className="text-nowrap"> Token)
											<TippyWrapper
												msg="The owner of future wNFT will be unable to transfer it"
												elClass="ml-1"
											></TippyWrapper>
											</span>
										</span>
									</label>
									<p><small className="text-muted">These options are&nbsp;irreversible. The custodian is&nbsp;accountable for&nbsp;any protocol-related actions.</small></p>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		)
	}

	const getFeeBlock = () => {

		if ( pageMode === 'simple' ) { return null; }
		if ( inputRules.noTransfer ) { return null; }

		return (
			<div className="c-wrap">
				<div className="c-wrap__header">
					<div className="h3">
						Transfer Fee
						<TippyWrapper
							msg="Amount of transfer fee. Sender has to pay this fee to transfer wNFT. So that fee amount must be approved to this contract before any wNFT transfer (marketplace trading)."
							elClass="ml-1"
						></TippyWrapper>
					</div>
				</div>
				<div className="c-wrap__form">
					<div className="row">
						<div className="col-md-7">
							<div className="select-group">
								<input
									className="input-control"
									type="text"
									placeholder="0.000"
									disabled={ subscriptionBlocked() || inputRules.noTransfer }
									value={ addThousandSeparator(inputTransferFeeAmount) }
									onChange={(e) => {
										let value = removeThousandSeparator(e.target.value);
										if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
											if ( new BigNumber(value).isNaN() ) {
												return;
											}
											value = new BigNumber(value).toString();
										}

										if ( inputAddWNFTChecked && value !== '' ) {
											setTimeout(() => { addCollateralRow(inputTransferFeeAddress, '0') }, 1);
										}

										setInputTransferFeeAmount(value);
									}}
									onBlur={(e) => {
										if ( e.target.value === '' ) {
											setCollaterals(
												collaterals.filter((item: CollateralItem) => { return !item.amount || !item.amount.eq(0) })
											);
										} else {
											if ( inputAddWNFTChecked ) {
												addCollateralRow(inputTransferFeeAddress, '0')
											}
										}
									}}
								/>
								<CoinSelector
									tokens        = { filterERC20ContractByPermissions({ enabledForFee: true }) }
									selectedToken = { inputTransferFeeAddress }
									onChange      = {(address: string) => {
										if ( inputAddWNFTChecked && inputTransferFeeAmount !== '' ) {
											addCollateralRow(address, '0')
										}

										setInputTransferFeeAddress(address);
									}}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

	const getNonRemovableAddress = () => {

		if ( inputTransferFeeAmount === '' ) { return undefined; }
		if ( new BigNumber(inputTransferFeeAmount).isNaN() ) { return undefined; }
		if ( new BigNumber(inputTransferFeeAmount).eq(0) ) { return undefined; }
		if ( !inputAddWNFTChecked ) { return undefined; }

		return inputTransferFeeAddress
	}
	const addCollateralRow = (addressInput: string, amountInput: string) => {

		if ( !currentChain ) { return; }

		let amount = new BigNumber(amountInput);
		if ( amount.isNaN() ) { return; }

		if ( addressInput === '' || addressInput === '0' || addressInput === '0x0000000000000000000000000000000000000000' ) {
			addressInput = '0x0000000000000000000000000000000000000000';
			amount = tokenToInt(amount, currentChain.decimals);
			if ( amount.gt(balanceNative) ) { return; }
		} else {

			let foundERC20 = erc20List.find((item) => {
				return item.contractAddress.toLowerCase() === addressInput.toLowerCase()
			});
			if ( !foundERC20 ) {
				requestERC20Token(addressInput, userAddress);
				foundERC20 = getNullERC20(addressInput);
			}
			if ( foundERC20 ) {
				amount = tokenToInt(amount, foundERC20.decimals || currentChain.decimals);
				if ( amount.gt(foundERC20.balance) ) { return; }
			}
		}

		setCollaterals((prevState) => {
			let collateralsUpdated = prevState.filter((item) => { return item.amount && !item.amount.eq(0) });
			const foundExisting = collateralsUpdated.find((item) => { return item.contractAddress.toLowerCase() === addressInput.toLowerCase() });
			let collateralToAdd: CollateralItem;

			if ( foundExisting ) {
				collateralToAdd = {
					assetType: foundExisting.assetType,
					contractAddress: foundExisting.contractAddress,
					amount: foundExisting.amount ? foundExisting.amount.plus(amount) : amount,
				}
			} else {
				collateralToAdd = {
					assetType: addressInput === '0x0000000000000000000000000000000000000000' ? _AssetType.native : _AssetType.ERC20,
					contractAddress: addressInput,
					amount: amount,
				}
			}

			collateralsUpdated = collateralsUpdated.filter((item) => { return item.contractAddress.toLowerCase() !== addressInput.toLowerCase() });
			return [
				...collateralsUpdated,
				collateralToAdd
			]
		})

		setInputCollateralAddress('');
		setInputCollateralAmount('');
	}
	const addCollateralDisabled = () => {
		if ( subscriptionBlocked() ) { return 'Add' }

		if ( collaterals.length + 1 > maxCollaterals ) {
			return 'Max collaterals'
		}

		if (
			inputCollateralAddress === '' ||
			!Web3.utils.isAddress(inputCollateralAddress)
		) { return 'Wrong address' }

		const amount = new BigNumber(inputCollateralAmount);
		if (
			!inputCollateralAmount ||
			amount.eq(0) ||
			amount.isNaN()
		) { return 'Empty amount' }

		return ''
	}
	const getCollateralAmountTitle = () => {

		if (
			inputCollateralAddress &&
			inputCollateralAddress !== '' &&
			inputCollateralAddress !== '0' &&
			inputCollateralAddress !== '0x0000000000000000000000000000000000000000'
		) {
			let foundERC20 = erc20List.find((item) => {
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
			});
			if ( !foundERC20 ) {
				if ( Web3.utils.isAddress(inputCollateralAddress) ) { requestERC20Token(inputCollateralAddress, userAddress); }

				return (
					<label className="input-label text-orange">
						<TippyWrapper
							msg="decimals is unknown; enter amount in wei"
						>
							<span>Amount*</span>
						</TippyWrapper>
						<TippyWrapper
							msg="Amount of tokens for every recipient"
						>
							<span className="i-tip ml-1"></span>
						</TippyWrapper>
					</label>
				)
			}
		}

		return (
			<label className="input-label">
				Amount
				<TippyWrapper
					msg="Amount of tokens for every recipient"
					elClass="ml-1"
				/>
			</label>
		)
	}
	const collateralOnDrop = (files: Array<File>) => {

		if ( subscriptionBlocked() ) { return; }

		files.forEach((item) => {
			const fileReader = new FileReader();
			fileReader.addEventListener('load', (e) => {
				let text = e.target?.result;
				if ( typeof(text) !== 'string' ) {
					return;
				}
				if ( !text ) { return; }
				let rows: Array<string> = [];
				if ( text.includes('\r\n') ) {
					rows = text.split('\r\n');
				}
				else {
					if ( text.includes('\n\r') ) {
						rows = text.split('\n\r');
					} else {
						if ( text.includes('\n') ) {
							rows = text.split('\n');
						}
					}
				}

				rows.forEach((iitem) => {
					try {
						const address = iitem.split(';')[0];
						// if ( !Web3.utils.isAddress(address) ) { return; }
						const amount = iitem.split(';')[1].replaceAll(',', '.');

						addCollateralRow( address, amount )
					} catch(e) { console.log('Cannot add row from file', iitem) }
				});

			});
			fileReader.readAsText(item)
		})
	}
	const getCollateralERC20Balance = () => {
		if ( !userAddress ) { return null; }
		if ( inputCollateralAddress === '' ) { return null; }

		const getNativeAmount = () => {
			const DECIMALS_TO_SHOW = 5;

			const amount = tokenToFloat(balanceNative, currentChain?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								setInputCollateralAmount( amount.toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => {
						setInputCollateralAmount( amount.toString() )
					}}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		if ( inputCollateralAddress === '0x0000000000000000000000000000000000000000' ) {
			return (
				<div className="c-add__max mt-2 mb-0">
					<div className="mt-1 mb-1">
						<span>Max: </span>
						{ getNativeAmount() }
					</div>
				</div>
			)
		}

		let foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundToken ) {
			if ( Web3.utils.isAddress(inputCollateralAddress) ) { requestERC20Token(inputCollateralAddress, userAddress); }
			return;
		}

		const allowance = foundToken.allowance.find((item) => { return item.allowanceTo.toLowerCase() === wrapperContract.toLowerCase() });

		const getERC20Amount = () => {
			const DECIMALS_TO_SHOW = 5;

			if ( !foundToken?.balance ) { return null; }
			const amount = tokenToFloat(foundToken.balance, foundToken?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								if ( !foundToken ) { return; }
								setInputCollateralAmount( tokenToFloat(foundToken.balance, foundToken?.decimals || 18).toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => { setInputCollateralAmount(amount.toString()) }}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}
		const getERC2Allowance = () => {
			const DECIMALS_TO_SHOW = 5;

			if ( !allowance?.amount ) { return null; }

			const amount = tokenToFloat(allowance.amount, foundToken?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								if ( !foundToken ) { return; }
								setInputCollateralAmount( amount.toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => {
						if ( !foundToken ) { return; }
						setInputCollateralAmount( amount.toString() )
					}}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		return (
			<div className="c-add__max mt-2 mb-0">
				<div className="mt-1 mb-1">
					<span>Max: </span>
					{ getERC20Amount() }
				</div>
				{
					allowance ? (
						<div>
							<span>Allowance: </span>
							{ getERC2Allowance() }
						</div>
					) : null
				}
			</div>
		)
	}
	const getCollateralBlock = () => {
		if ( !currentChain ) { return null; }
		if ( inputRules.noAddCollateral ) { return null; }

		return (
			<Dropzone
				onDrop={(files) => { collateralOnDrop(files) }}
				accept={{ 'text/csv': ['.txt', '.csv'] }}
				noClick={ true }
				noKeyboard={ true }
			>
				{
					({
						getRootProps, getInputProps, isDragActive, open
					}) => (
						<div {...getRootProps({ className: `c-wrap c-b-wrap ${isDragActive && !subscriptionBlocked() ? 'file-dragged' : ''}` })}>

							<div className="upload-poopover">
								<div className="inner">
									<div className="h3">Drop your file here</div>
								</div>
							</div>

							<input {...getInputProps()} />
							<div className="c-wrap__header" ref={ scrollable.collaterals }>
								<div className="h3">Collateral for each wNFT</div>
								<div className="c-wrap__info">
									<div>Enter <span className="text-grad">address = 0</span> to add { currentChain.symbol }</div>
									<img src={ icon_i_attention } alt="" />
								</div>
							</div>
							<div className="c-wrap__form">
								<div className="row">
									<div className="col col-12 col-md-5">
										<label className="input-label">{ currentChain.EIPPrefix }-20 Address</label>
										<div className={ `select-group ${ subscriptionBlocked() ? 'disabled' : '' }` } >
											<input
												className="input-control"
												type="text"
												placeholder="0x000"
												disabled={ subscriptionBlocked() }
												value={ inputCollateralAddress }
												onChange={(e) => {
													const address = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
													if ( Web3.utils.isAddress(address) ) {
														let foundERC20 = erc20List.find((item) => {
															if ( !item.contractAddress ) { return false; }
															return item.contractAddress.toLowerCase() === address.toLowerCase()
														});
														if ( !foundERC20 ) {
															foundERC20 = getNullERC20(address);
														}
														requestERC20Token(address, userAddress);
													}
													setInputCollateralAddress(address);
												}}
												onKeyPress={(e) => {
													if ( e.defaultPrevented)         { return; }
													if ( !!addCollateralDisabled() ) { return; }
													if ( e.key !== 'Enter' )         { return; }

													addCollateralRow( inputCollateralAddress, inputCollateralAmount )
												}}
											/>

											<CoinSelector
												tokens        = { filterERC20ContractByPermissions({ enabledForCollateral: true }) }
												selectedToken = { inputCollateralAddress }
												onChange      = {(address: string) => {
													setInputCollateralAddress(address);
												}}
											/>

										</div>
									</div>
									<div className="col col-12 col-md-4">
										{ getCollateralAmountTitle() }
										<input
											className="input-control"
											type="text"
											placeholder=""
											disabled={ subscriptionBlocked() }
											value={ addThousandSeparator(inputCollateralAmount) }
											onChange={(e) => {
												let value = removeThousandSeparator(e.target.value);
												if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
													if ( new BigNumber(value).isNaN() ) { return; }
													value = new BigNumber(value).toString();
												}
												setInputCollateralAmount(value);
											}}
											onKeyPress={(e) => {
												if ( e.defaultPrevented)         { return; }
												if ( !!addCollateralDisabled() ) { return; }
												if ( e.key !== 'Enter' )         { return; }

												addCollateralRow( inputCollateralAddress, inputCollateralAmount )
											}}
										/>
										{ getCollateralERC20Balance() }
									</div>
									<div className="col col-12 col-md-3">
										<label className="input-label">&nbsp;</label>
										<button
											className="btn btn-grad"
											disabled={ !!addCollateralDisabled() }
											onClick={ (e) => {
												addCollateralRow( inputCollateralAddress, inputCollateralAmount )
											} }
										>Add</button>
									</div>
								</div>

								<div className="c-wrap__upload-row">
									<button
										className="btn-link btn-csv mr-md-3"
										onClick={ open }
									>Click to upload a CSV file</button>
									<span className="d-none d-md-inline">
										<span className="mr-3">or </span>
										drag and drop it here
									</span>
								</div>
								<small className="text-muted">Use semicolon as a divider in file</small>

							</div>

							<CollateralViewer
								collaterals={ collaterals }
								nonRemovableAddress={ getNonRemovableAddress() }
								removeRow={(item: CollateralItem) => {
									if  ( item.contractAddress.toLowerCase() === inputTransferFeeAddress.toLowerCase() ) {
										setCollaterals([
											...collaterals.filter((iitem) => { return iitem.contractAddress.toLowerCase() !== item.contractAddress.toLowerCase() }),
											{
												...item,
												amount: new BigNumber(0)
											}
										]);
									} else {
										setCollaterals([
											...collaterals.filter((iitem) => { return iitem.contractAddress.toLowerCase() !== item.contractAddress.toLowerCase() }),
										]);
									}
								}}
								width={ 'wide' }
							/>
							{ collaterals.length && WNFTRecipients.length ? ( <CollateralSummary collaterals={ collaterals } recipients={ WNFTRecipients.length } rows={3} /> ) : null }
						</div>
					)
				}
			</Dropzone>
		)
	}

	const recalcPercents = (recs: Array<RoyaltyInput>) => {
		let sum = 10000;
		const recLength = recs.length;
		return recs.map((item: RoyaltyInput): RoyaltyInput => {
			let percentToAdd;
			if ( sum > Math.ceil(10000 / recLength) + recLength ) {
				percentToAdd = new BigNumber(Math.floor(10000 / recLength)).dividedBy(100);
				sum = sum - Math.floor(10000 / recLength);
			} else {
				percentToAdd = new BigNumber(sum).dividedBy(100);
				sum = 0;
			}
			return {
				...item,
				percent: percentToAdd.toFixed(2)
			}
		});
	}
	const addRoyaltyRecipient = (address: string | undefined) => {

		const foundRoyaltyRecipient = royaltyRecipients.find((item) => {
			if ( !item.address && !address ) { return true; }
			if ( !item.address ) { return false; }
			if ( !address ) { return false; }
			return item.address.toLowerCase() === address.toLowerCase()
		});
		if ( foundRoyaltyRecipient ) { return; }

		let collateralsUpdated = collaterals;
		if ( !address ) {
			const foundFeeToken = collateralsUpdated.find((item: CollateralItem) => { return item.contractAddress.toLowerCase() === inputTransferFeeAddress.toLowerCase() });
			if ( !foundFeeToken ) {
				collateralsUpdated = [
					...collateralsUpdated,
					{
						assetType: _AssetType.ERC20,
						contractAddress: inputTransferFeeAddress,
						amount: new BigNumber(0),
					}
				]
			}
		}

		setRoyaltyRecipients([
			...royaltyRecipients,
			{
				address: address,
				percent: '0',
				timeAdded: new Date().getTime(),
			}
		]);
		setCollaterals(collateralsUpdated);
		setInputRoyaltyRecipientAddress('');
		setShowErrors(showErrors.filter((item) => { return item !== 'royalty' }));
	}
	const removeRoyaltyRecipient = (address: string | undefined) => {
		if ( address ) {
			setRoyaltyRecipients(
				royaltyRecipients.filter((item) => {
					if ( !item.address ) { return true; }
					return item.address.toLowerCase() !== address.toLowerCase()
				})
			)
		} else {
			setRoyaltyRecipients(
				royaltyRecipients.filter((item) => {
					return !!item.address
				})
			)
			setInputAddWNFTChecked(false);
		}

		let collateralsUpdated = collaterals;
		if ( !address ) {
			setCollaterals(
				collateralsUpdated.filter((item: CollateralItem) => { return !item.amount || !item.amount.eq(0) })
			);
		}

		setInputRoyaltyRecipientAddress('');
	}
	const getAddRoyaltyRecipientBtn = () => {
		return (
			<button
				className="btn btn-grad"
				type="submit"
				disabled={ isAddRoyaltyRecipientDisabled() }
				onClick={() => { addRoyaltyRecipient(inputRoyaltyRecipientAddress) }}
			>Add</button>
		)
	}
	const isAddRoyaltyRecipientDisabled = () => {
		if ( inputRoyaltyRecipientAddress === '' ) { return true; }
		if ( !Web3.utils.isAddress(inputRoyaltyRecipientAddress) ) { return true; }
		return false;
	}
	const getRoyaltyRecipientCalcedRoyalty = (item: RoyaltyInput) => {
		if ( inputTransferFeeAmount === '' ) { return '—' }

		const fee = new BigNumber(inputTransferFeeAmount);
		if ( !fee || fee.isNaN() ) { return '—' }

		const percent = new BigNumber(item.percent).dividedBy(100);

		return fee.multipliedBy(percent).toString()
	}
	const getRoyaltyRecipientRow = (item: RoyaltyInput, idx: number) => {
		return (
			<div
				key={ item.address || 'none' }
				className="item"
			>
				<div className="row">
					<div className="mb-2 col-12 col-md-1">#{ idx }</div>
					<div className="mb-3 mb-md-2 col-12 col-md-4">
						<span className="col-legend">Token: </span>
						<span className="text-break">{ item.address ? compactString(item.address) : 'the wNFT' }</span>
					</div>
					<div className="mb-2 col-12 col-md-2">
						<div className="tb-input tb-percent">
							<input
								className="input-control"
								type="text"
								value={ item.percent.toString() }
								onFocus={(e) => { e.target.select() }}
								onChange={(e) => {

									let value = e.target.value.replaceAll(' ', '').replaceAll(',', '.');
									const valueParsed = new BigNumber(value);

									if ( value === '' ) {
										value = '';
									} else {
										if ( valueParsed.isNaN() ) { return; }
										if ( valueParsed.gt(100) ) { return; }
									}

									// the wNFT
									if ( !item.address ) {
										setRoyaltyRecipients([
											...royaltyRecipients.filter((iitem) => { return !!iitem.address }),
											{
												address: undefined,
												percent: value,
											}
										]);
										setShowErrors(showErrors.filter((item) => { return item !== 'royalty' }));
										return;
									}

									// plain address
									const foundRoyaltyRecipient = royaltyRecipients.filter((iitem) => {
										if ( !iitem.address ) { return false; }
										if ( !item.address ) { return false; }
										return iitem.address.toLowerCase() === item.address.toLowerCase()
									});
									if ( foundRoyaltyRecipient.length ) {
										setRoyaltyRecipients([
											...royaltyRecipients.filter((iitem) => {
												if ( !iitem.address ) { return true; }
												if ( !item.address ) { return true; }
												return iitem.address.toLowerCase() !== item.address.toLowerCase()
											}),
											{
												address: item.address,
												percent: value,
												timeAdded: foundRoyaltyRecipient[0].timeAdded
											},
										]);
										setShowErrors(showErrors.filter((item) => { return item !== 'royalty' }));
									}
								}}
								onBlur={() => {
									setRoyaltyRecipients(royaltyRecipients.map((item) => {
											return {
												...item,
												percent: new BigNumber(item.percent).toFixed(2, BigNumber.ROUND_DOWN)
											}
										})
									);
								}}
							/>
							<TippyWrapper
								msg="Percent of royalty from the transfer fee amount"
								elClass="ml-1"
							></TippyWrapper>
						</div>
					</div>
					<div className="mb-2 col-12 col-md-4">
						<span className="col-legend">Amount: </span>
						<span className="text-break">{ getRoyaltyRecipientCalcedRoyalty(item) }</span>
					</div>
					{
						item.address ? (
							<button
								className="btn-del"
								onClick={() => { removeRoyaltyRecipient(item.address) }}
							><img src={ icon_i_del } alt="" /></button>
						): null
					}
				</div>
			</div>
		)
	}
	const getDistributionAlert = () => {

		const showError = showErrors.find((item) => { return item === 'royalty' })
		if ( !showError ) { return null; }

		if ( inputTransferFeeAmount !== '' ) {
			const sumRoyaltyPercent = royaltyRecipients.reduce((acc, item) => {
				const percentToAdd = new BigNumber(item.percent);
				if ( percentToAdd.isNaN() ) { return acc }
				return acc.plus(percentToAdd)
			}, new BigNumber(0));
			if ( !sumRoyaltyPercent.eq(100) ) { return ( <div className="alert alert-error mt-3">Royalty percent is not equal 100</div> ) }
		}

		return null;
	}
	const getRoyaltyBlock = () => {

		if ( pageMode === 'simple' ) { return null; }
		if ( inputRules.noTransfer ) { return null; }
		if ( inputTransferFeeAmount === '' ) { return null; }
		if ( inputTransferFeeAddress === '' ) { return null; }

		const showError = showErrors.find((item) => { return item === 'emptyRoyaltyRecipients' })

		return (
			<div className="c-wrap">
				<div className="c-wrap__header d-block">
					<div className="row align-items-center">
						<div className="col-12 col-md-auto">
							<div
								className="h3 mt-0"
								ref={ scrollable.royalty }
							>Royalty Recipients</div>
						</div>
						<div className="col-12 col-md-auto">
							<label className="checkbox">
								<input
									type="checkbox"
									disabled={
										inputTransferFeeAmount === ''  ||
										inputTransferFeeAddress === '' ||
										inputRules.noTransfer ||
										inputRules.noAddCollateral
									}
									checked={ inputAddWNFTChecked }
									onChange={(e) => {
										if ( e.target.checked ) {
											setInputAddWNFTChecked(e.target.checked);
											setShowErrors(showErrors.filter((item) => { return item !== 'emptyRoyaltyRecipients' }));
											setTimeout(() => { addRoyaltyRecipient(undefined) }, 1);
										} else {
											setInputAddWNFTChecked(e.target.checked);
											setShowErrors(showErrors.filter((item) => { return item !== 'emptyRoyaltyRecipients' }));
											setTimeout(() => { removeRoyaltyRecipient(undefined) }, 1);
										}
									}}
								/>
								<span className="check"> </span>
								<span className="check-text">Add the wNFT to the recipients list</span>
							</label>
						</div>
					</div>
				</div>
				<div className="c-wrap__form">
					<div className="row mb-1">
						<div className="col col-12 col-md-6">
							<label className="input-label">
								Address
								<TippyWrapper
									msg="Address of royalty income reciever"
								></TippyWrapper>
							</label>
							<input
								className={`input-control ${ showError ? 'has-error' : '' }`}
								type="text"
								placeholder="Paste here"
								value={ inputRoyaltyRecipientAddress }
								onChange={(e) => {
									setInputRoyaltyRecipientAddress(e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, ""));
									setShowErrors(showErrors.filter((item) => { return item !== 'emptyRoyaltyRecipients' }));
								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented)                 { return; }
									if ( !!isAddRoyaltyRecipientDisabled() ) { return; }
									if ( e.key !== 'Enter' )                 { return; }

									addRoyaltyRecipient(inputRoyaltyRecipientAddress)
								}}
							/>
						</div>
						<div className="col col-12 col-md-2">
							<label className="input-label">&nbsp;</label>
							{ getAddRoyaltyRecipientBtn() }
						</div>
					</div>

					{
						royaltyRecipients.length ?
						(
							<div className="row">
								<div className="col-sm-6 col-md-3">
									<button
										className="btn btn-sm btn-gray"
										onClick={() => {
											setRoyaltyRecipients(recalcPercents(royaltyRecipients));
											setShowErrors(showErrors.filter((item) => { return item !== 'emptyRoyaltyRecipients' }));
										}}
									>Divide percents evenly</button>
								</div>
							</div>
						) : null
					}

					{ getDistributionAlert() }

				</div>
				<div className="c-wrap__table mt-3">
					{
						royaltyRecipients
							.sort((item, prev) => {

								if ( !item.address ) {
									return -1
								}
								if ( !prev.address ) {
									return 1
								}

								// if ( item.address < prev.address ) { return -1 }
								// if ( item.address > prev.address ) { return  1 }

								if ( !item.timeAdded ) {
									return -1
								}
								if ( !prev.timeAdded ) {
									return 1
								}
								if ( item.timeAdded < prev.timeAdded ) { return -1 }
								if ( item.timeAdded > prev.timeAdded ) { return  1 }

								return 0;

							})
							.map((item, idx) => { return getRoyaltyRecipientRow(item, idx + 1) })
					}
				</div>
			</div>
		)
	}

	const getWNFTRecipientRow = (WNFTRecipient: SAFTRecipientItem, position: number) => {
		return (
			<div className="item" key={ WNFTRecipient.timeAdded }>
				<div className="row">
					<div className="col-md-1 mb-2">#{ position }</div>
					<div className="col-md-5 mb-2">
						<span className="text-break">{ WNFTRecipient.userAddress }</span>
					</div>
					<div className="col-md-5 mb-2">{ wrapWith !== 'empty' ? `ID ${ WNFTRecipient.tokenId }` : null }</div>
					<button
						className="btn-del"
						onClick={(e) => {
							const filteredWNFTRecipients = WNFTRecipients.filter((item) => { return item.timeAdded !== WNFTRecipient.timeAdded });
							setWNFTRecipients(filteredWNFTRecipients)
						}}
					>
						<img src={ icon_i_del } alt="" />
					</button>
				</div>
			</div>
		)
	}
	const addWNFTRecipientDisabled = () => {

		if ( subscriptionBlocked() ) { return 'Add' }
		if ( inputWNFTRecipientAddress === '' ) { return 'Add' }

		if ( !Web3.utils.isAddress(inputWNFTRecipientAddress) ) {
			return 'Wrong address'
		}

		if ( !inputWNFTRecipientTokenId && wrapWith !== 'empty' ) {
			return 'Empty token id'
		}

		if ( inputWNFTRecipientTokenId ) {
			const foundAdded = WNFTRecipients.find((iitem) => {
				if ( !iitem.tokenId ) { return true }
				return iitem.tokenId === inputWNFTRecipientTokenId;
			});
			if ( foundAdded ) { return 'Already added' }
		}

		return ''
	}
	const WNFTRecipientsOnDrop = (files: Array<File>) => {

		if ( subscriptionBlocked() ) { return; }

		files.forEach((item) => {
			const fileReader = new FileReader();
			fileReader.addEventListener('load', (e) => {
				let text = e.target?.result;
				if ( typeof(text) !== 'string' ) {
					return;
				}
				if ( !text ) { return; }

				let rows: Array<string> = [];
				if ( text.includes('\r\n') ) {
					rows = text.split('\r\n');
				}
				else {
					if ( text.includes('\n\r') ) {
						rows = text.split('\n\r');
					} else {
						if ( text.includes('\n') ) {
							rows = text.split('\n');
					}
					}
				}

				rows.forEach((iitem, idx) => {
					if ( Web3.utils.isAddress(iitem)
					) {
						// whole string is address
						addWNFTRecipientRow(iitem, '', idx);
					} else {
						// two comma separated values
						try {
							const address = iitem.split(';')[0];
							const tokenId = iitem.split(';')[1].replaceAll(',', '.');

							if ( !Web3.utils.isAddress(address) ) { return null; }

							addWNFTRecipientRow(address, tokenId, idx);
						} catch(e) { console.log('Cannot add row from file', iitem) }
					}
				});

			});
			fileReader.readAsText(item)
		})
	}
	const addWNFTRecipientRow = (address: string, tokenId: string, idx?: number) => {
		setWNFTRecipients((prevState) => {

			const foundToken = WNFTRecipients.filter((item) => {
				if ( !item.tokenId ) { return false; }
				return item.tokenId === tokenId;
			});
			if ( foundToken.length ) { return prevState; }

			return [
				...prevState,//.filter((item) => { return item.userAddress.toLowerCase() !== address.toLowerCase() }),
				{
					timeAdded: new Date().getTime() + (idx || 0),
					userAddress: address,
					tokenId: tokenId
				}
			]
		});
		setInputWNFTRecipientAddress('');
		setInputWNFTRecipientTokenId('');

	}
	const getFetchedTokensList = () => {
		const foundContract = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase() });
		if ( !foundContract ) { return []; }

		return foundContract.tokens
			.filter((item) => {
				const foundAdded = WNFTRecipients.find((iitem) => {
					if ( !iitem.tokenId ) { return true }
					return iitem.tokenId === item.tokenId
				});
				if ( foundAdded ) { return false; }
				return true;
			})
			.map((item) => {
				return { value: item.tokenId || '', label: compactString(item.tokenId || '', 3) }
			})
	}
	const getWNFTRecipientsBlock = () => {
		return (
			<Dropzone
				onDrop={(files) => { WNFTRecipientsOnDrop(files) }}
				accept={{ 'text/csv': ['.txt', '.csv'] }}

				noClick={ true }
				noKeyboard={ true }
			>
				{
					({
						getRootProps, getInputProps, isDragActive, open
					}) => (
						<div {...getRootProps({ className: `c-wrap c-b-wrap ${isDragActive && !subscriptionBlocked() ? 'file-dragged' : ''}` })}>

							<div className="upload-poopover">
								<div className="inner">
									<div className="h3">Drop your file here</div>
								</div>
							</div>

							<input {...getInputProps()} />
							<div className="c-wrap__header" ref={ scrollable.recipients }>
								<div className="h3">wNFT Recipients</div>
								{/* {
									wrapWith === 'predefined' ? (
										<button
											className="btn btn-gray"
											onClick={() => {
												// setCurrentPage('mint');
												window.location.href = '/mintnew';
										}}
											disabled={ subscriptionBlocked() }
										>Mint more tokens</button>
									) : null
								} */}
							</div>
							<div className="c-wrap__form">
								<div className="row">
									<div className="col col-12 col-md-5">
										<label className="input-label">Recipient Address</label>
										<input
											className="input-control"
											type="text"
											placeholder="Paste here"
											value={ inputWNFTRecipientAddress }
											disabled={ subscriptionBlocked() }
											onChange={(e) => { setInputWNFTRecipientAddress( e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "") ) }}
											onKeyPress={(e) => {
												if ( e.defaultPrevented)            { return; }
												if ( !!addWNFTRecipientDisabled() ) { return; }
												if ( e.key !== 'Enter' )            { return; }

												addWNFTRecipientRow(inputWNFTRecipientAddress, inputWNFTRecipientTokenId);
											}}
										/>
									</div>
									<div className="col col-12 col-md-4">
										<label className="input-label">Original NFT token ID</label>
										<InputWithOptions
											placeholder=""
											disabled={ wrapWith === 'empty' || subscriptionBlocked() }
											value={ inputWNFTRecipientTokenId }
											onChange={(e) => { setInputWNFTRecipientTokenId( e.target.value.toLowerCase().replace(/[^0-9]/g, "") ) }}
											onSelect={(e) => {
												setInputWNFTRecipientTokenId( e.value.toLowerCase().replace(/[^0-9]/g, "") )
											}}
											onKeyPress={(e) => {
												if ( e.defaultPrevented)            { return; }
												if ( !!addWNFTRecipientDisabled() ) { return; }
												if ( e.key !== 'Enter' )            { return; }

												addWNFTRecipientRow(inputWNFTRecipientAddress, inputWNFTRecipientTokenId);
											}}
											options={
												getFetchedTokensList()
											}
											showArrow={ false }
										/>
									</div>
									<div className="col col-12 col-md-3">
										<label className="input-label">&nbsp;</label>
										<button
											className="btn btn-grad"
											disabled={ !!addWNFTRecipientDisabled() }
											onClick={(e) => {
												addWNFTRecipientRow(inputWNFTRecipientAddress, inputWNFTRecipientTokenId);
											}}
										>{ addWNFTRecipientDisabled() ? addWNFTRecipientDisabled() : 'Add' }</button>
									</div>
								</div>

								<div className="c-wrap__upload-row">
									<button
										className="btn-link btn-csv mr-md-3"
										onClick={ open }
									>Click to upload a CSV file</button>
									<span className="d-none d-md-inline">
										<span className="mr-3">or</span>
										drag and drop it here
									</span>
								</div>
								<small className="text-muted">Use semicolon as a divider in file</small>

							</div>
							<div className="c-wrap__table mt-3">
								{
									WNFTRecipients.map((item, idx) => { return getWNFTRecipientRow(item, idx + 1) })
								}
							</div>
						</div>
					)
				}
			</Dropzone>
		)
	}

	const hasErrors = () => {

		const errors: Array<{ msg: string, block: any, showError?: string }> = [];

		// ----- ORIGINAL TOKEN -----
		if (
			wrapWith !== 'empty' &&
			inputOriginalTokenAddress === ''
		) {
			errors.push({ msg: 'Enter original contract address or set empty', block: scrollable.originalToken })
		}

		if (
			wrapWith !== 'empty' &&
			inputOriginalTokenAddress !== '' &&
			!isAddress(inputOriginalTokenAddress)
		) {
			errors.push({ msg: 'Bad address format', block: scrollable.originalToken })
		}

		const foundContract = fetchedTokens.find((item) => { return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase() })
		if (
			wrapWith === 'own' &&
			inputOriginalTokenAddress !== '' &&
			foundContract && foundContract.badContract
		) {
			errors.push({ msg: 'Cannot load data from contract', block: scrollable.originalToken })
		}

		if ( originalTokensBlacklist ) {
			const foundBlacklistItem = originalTokensBlacklist.find((item: any) => {
				if ( typeof(item) === 'string' ) {
					return item.toLowerCase() === inputOriginalTokenAddress.toLowerCase()
				}
				return item.contractAddress.toLowerCase() === inputOriginalTokenAddress.toLowerCase()
			});
			if ( foundBlacklistItem ) {
				errors.push({ msg: 'Original token address is blacklisted', block: scrollable.originalToken })
			}
		}

		if (
			inputOutAssetType === _AssetType.ERC1155 &&
			( inputCopies <= 0 )
		) {
			errors.push({ msg: 'Enter wnft token amount', block: scrollable.standart, showError: 'standart' })
		}
		// ----- END ORIGINAL TOKEN -----

		// ----- COLLATERALS -----'
		if ( inputRules.noAddCollateral && collaterals.length ) {
			errors.push({ msg: 'Cannot add collaterals due to rule', block: scrollable.collaterals })
		}
		// ----- END COLLATERALS -----s

		// ----- RECIPIENTS -----
		if ( !WNFTRecipients.length ) {
			errors.push({ msg: 'Empty NFT recipients list', block: scrollable.recipients })
		}
		// ----- END RECIPIENTS -----

		// ----- ROYALTIES -----
		if ( !inputRules.noTransfer ) {
			if (
				( inputTransferFeeAmount !== '' && !(new BigNumber(inputTransferFeeAmount).isNaN()) && !(new BigNumber(inputTransferFeeAmount).eq(0)) )
				&& !royaltyRecipients.length
			) {
				errors.push({ msg: 'Empty royalty recipients list', block: scrollable.royalty, showError: 'emptyRecipients' })
			} else {
				if ( ( inputTransferFeeAmount !== '' && !(new BigNumber(inputTransferFeeAmount).isNaN()) && !(new BigNumber(inputTransferFeeAmount).eq(0)) ) ) {
					const sumRoyaltyPercent = royaltyRecipients.reduce((acc, item) => {
						const percentToAdd = new BigNumber(item.percent);
						if ( percentToAdd.isNaN() ) { return acc }
						return acc.plus(percentToAdd)
					}, new BigNumber(0));
					if ( !sumRoyaltyPercent.eq(100) ) { errors.push({ msg: 'Royalty percent is not equal 100', block: scrollable.royalty, showError: 'royalty' }) }
				}
			}
		}
		// ----- END ROYALTIES -----

		// ----- LOCKS -----
		if (
			inputTimeLockDays !== ''
		) {
			const days = new BigNumber(inputTimeLockDays);
			if ( days.isNaN() || days.eq(0) ) {
				errors.push({ msg: 'Empty time unlock', block: scrollable.timelock, showError: 'timeUnlock' })
			}
		}

		// ----- END LOCKS -----

		// collateralErrors.forEach((item) => { errors.push({ msg: `Error with collateral: ${compactString(item.address)} - ${item.tokenId || ''}. ${item.msg}`, block: scrollable.collaterals }) })

		return errors;
	}
	const getErrorsBlock = () => {
		const errors = hasErrors();

		if ( !errors.length ) { return null; }

		return (
			<div className="c-errors mt-4">
				<div className="mb-3">To enable the Wrap button, correct the following errors:</div>
				<ul>
				{
					errors.map((item) => {
						return (
							<li key={ item.msg }>
								<button
									className="btn-link"
									onClick={() => {
										item.block.current.scrollIntoView();
										if ( item.showError ) {
											setShowErrors([
												...showErrors.filter((iitem) => { return iitem !== item.showError }),
												item.showError
											]);
										}
									}}
								>
									{ item.msg }
								</button>
							</li>
						)
					})
				}
				</ul>
			</div>
		)
	}

	const getClearBtn = () => {
		return (
			<button
				className="btn btn-yellow w-100"
				onClick={() => {
					setWrapWith('empty');;
					setInputOriginalTokenAddress('');
					setInputTimeLockDays('');
					setInputOutAssetType(_AssetType.ERC721);
					setInputCopies(2);
					setInputRules(decodeRules('0000'));
					setInputTransferFeeAddress('');
					setInputTransferFeeAmount('');
					setInputRoyaltyRecipientAddress('');
					setInputAddWNFTChecked(false);
					setCollaterals([]);
					setInputCollateralAddress('');
					setInputCollateralAmount('');
				}}
			>Clear form</button>
		)
	}

	const createAdvancedLoaderSAFT = (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {
		const loaderStages: Array<AdvancedLoaderStageType> = []
		if ( wrapWith !== 'empty' ) {
			loaderStages.push({
				id: 'approveoriginal',
				sortOrder: 1,
				text: `Approving tokens of contract to wrap`,
				status: _AdvancedLoadingStatus.queued
			});
		}

		const qtyERC20Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC20
		});
		if ( qtyERC20Collaterals.length ) {
			loaderStages.push({
				id: 'approve20collateral',
				sortOrder: 10,
				text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC20Collaterals.length,
			});
		}
		const qtyERC721Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC721
		});
		if ( qtyERC721Collaterals.length ) {
			loaderStages.push({
				id: 'approve721collateral',
				sortOrder: 11,
				text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC721Collaterals.length,
			});
		}
		const qtyERC1155Collaterals = collaterals.filter((item) => {
			return item.assetType === _AssetType.ERC1155
		});
		if ( qtyERC1155Collaterals.length ) {
			loaderStages.push({
				id: 'approve1155collateral',
				sortOrder: 12,
				text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyERC1155Collaterals.length,
			});
		}

		const qtyWNFTCollateralFees = collaterals.filter((item) => {
			return addressIsStorage( item.contractAddress );
		});
		if ( qtyWNFTCollateralFees.length ) {
			loaderStages.push({
				id: 'approvewnftcollateralfees',
				sortOrder: 15,
				text: 'Approving WNFT collateral fees',
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: qtyWNFTCollateralFees.length,
			});
		}

		loaderStages.push({
			id: 'wrap',
			sortOrder: 20,
			text: 'Wrapping tokens',
			status: _AdvancedLoadingStatus.queued
		});

		const advLoader = {
			title: 'Waiting to create',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);
	}
	const approveOriginalTokens = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {
		if ( wrapWith === 'empty' ) { return; }

		updateStepAdvancedLoader({
			id: 'approveoriginal',
			status: _AdvancedLoadingStatus.loading,
		});

		const wrapperToUse = userStorageToWrap ? userWrapper : wrapperContract;
		if ( !wrapperToUse ) { return; }

		const isApproved = await checkApprovalForAllERC721(_currentChain.chainId, inputOriginalTokenAddress, _userAddress, wrapperToUse);
		if ( !isApproved ) {
			try {
				if ( isMultisig ) {
					await setApprovalForAllERC721Multisig(_web3, inputOriginalTokenAddress, _userAddress, wrapperToUse)
				} else {
					await setApprovalForAllERC721(_web3, inputOriginalTokenAddress, _userAddress, wrapperToUse)
				}
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot approve tokens to wrap`,
					details: [
						`Contract address: ${inputOriginalTokenAddress}`,
						`User address: ${_userAddress}`,
						'',
						e.message || e,
					]
				});
				throw new Error();
			}
		}
		updateStepAdvancedLoader({
			id: 'approveoriginal',
			status: _AdvancedLoadingStatus.complete,
		});
	}
	const approve20Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc20CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC20 });
		if ( !erc20CollateralsToCheck.length ) { return; }

		const wrapperToUse = userStorageToWrap ? userWrapper : wrapperContract;
		if ( !wrapperToUse ) { return; }

		for (let idx = 0; idx < erc20CollateralsToCheck.length; idx++) {
			const item = erc20CollateralsToCheck[idx];
			let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() });
			if ( !foundToken ) {
				foundToken = getNullERC20(item.contractAddress);
			}

			updateStepAdvancedLoader({
				id: 'approve20collateral',
				status: _AdvancedLoadingStatus.loading,
				text: `Approving ${ foundToken.symbol }`,
				current: idx + 1,
			});

			if ( !item.amount ) { continue; }
			if ( item.contractAddress.toLowerCase() === techToken.toLowerCase() ) { continue; }

			const amountToCheck = item.amount.multipliedBy(WNFTRecipients.length);

			const balance = await getERC20BalanceFromChain(_currentChain.chainId, item.contractAddress, _userAddress, wrapperToUse);
			if ( balance.balance.lt(amountToCheck) ) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${item.contractAddress}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
					]
				});
				throw new Error();
			}
			if ( !balance.allowance || balance.allowance.amount.lt(amountToCheck) ) {
				try {
					if ( isMultisig ) {
						await makeERC20AllowanceMultisig(_web3, item.contractAddress, _userAddress, amountToCheck, wrapperToUse);
					} else {
						await makeERC20Allowance(_web3, item.contractAddress, _userAddress, amountToCheck, wrapperToUse);
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve20collateral',
			text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});

	}
	const approve721Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc721CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC721 });
		if ( !erc721CollateralsToCheck.length ) { return; }

		const wrapperToUse = userStorageToWrap ? userWrapper : wrapperContract;
		if ( !wrapperToUse ) { return; }

		for (let idx = 0; idx < erc721CollateralsToCheck.length; idx++) {
			const item = erc721CollateralsToCheck[idx];

			if ( !item.tokenId ) { continue; }

			updateStepAdvancedLoader({
				id: 'approve721collateral',
				text: `Approving ${_currentChain.EIPPrefix}-721: ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			const isApproved = await checkApprovalERC721(_currentChain.chainId, item.contractAddress, item.tokenId, _userAddress, wrapperToUse);
			if ( !isApproved ) {
				try {
					if ( isMultisig ) {
						await setApprovalERC721Multisig(_web3, item.contractAddress, item.tokenId, _userAddress, wrapperToUse)
					} else {
						await setApprovalERC721(_web3, item.contractAddress, item.tokenId, _userAddress, wrapperToUse)
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-721`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`Token id: ${item.tokenId}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve721collateral',
			text: `Approving ${_currentChain.EIPPrefix}-721 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});
	}
	const approve1155Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const erc1155CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC1155 });
		if ( !erc1155CollateralsToCheck.length ) { return; }

		const wrapperToUse = userStorageToWrap ? userWrapper : wrapperContract;
		if ( !wrapperToUse ) { return; }

		for (let idx = 0; idx < erc1155CollateralsToCheck.length; idx++) {
			const item = erc1155CollateralsToCheck[idx];

			updateStepAdvancedLoader({
				id: 'approve1155collateral',
				text: `Approving ${_currentChain.EIPPrefix}-1155: ${compactString(item.contractAddress)}`,
				status: _AdvancedLoadingStatus.loading
			});

			const isApproved = await checkApprovalERC1155(_currentChain.chainId, item.contractAddress, _userAddress, wrapperToUse);
			if ( !isApproved ) {
				try {
					if ( isMultisig ) {
						await setApprovalERC1155Multisig(_web3, item.contractAddress, _userAddress, wrapperToUse)
					} else {
						await setApprovalERC1155(_web3, item.contractAddress, _userAddress, wrapperToUse)
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${_currentChain.EIPPrefix}-1155`,
						details: [
							`Contract address: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve1155collateral',
			text: `Approving ${_currentChain.EIPPrefix}-1155 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});
	}
	const approveWNFTFees = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		const wnftCollateralsToCheck = collaterals.filter((item) => { return addressIsStorage(item.contractAddress) });
		if ( !wnftCollateralsToCheck.length ) { return; }

		const wrapperToUse = userStorageToWrap ? userWrapper : wrapperContract;
		if ( !wrapperToUse ) { return; }

		for (let idx = 0; idx < wnftCollateralsToCheck.length; idx++) {
			const item = wnftCollateralsToCheck[idx];

			if ( !item.tokenId ) { continue; }

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving fee of ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			const wnft = await getWNFTById(_currentChain.chainId, item.contractAddress, item.tokenId);
			if ( !wnft || !wnft.fees.length ) { continue; }

			const fee = wnft.fees[0];

			if ( fee.token.toLowerCase() === techToken.toLowerCase() ) { continue; }

			let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase() });
			if ( !foundToken ) { foundToken = getNullERC20(fee.token) }

			updateStepAdvancedLoader({
				id: 'approvewnftcollateralfees',
				text: `Approving (${foundToken.symbol}) of ${compactString(item.contractAddress)}: ${item.tokenId}`,
				status: _AdvancedLoadingStatus.loading
			});

			// erc20 collateral of same token
			// without summing this tx will overwrite approval amount
			const erc20Collateral = collaterals.find((iitem) => { return iitem.contractAddress.toLowerCase() === fee.token.toLowerCase() });
			const amountToCheck = fee.value.plus(erc20Collateral?.amount?.multipliedBy(WNFTRecipients.length) || new BigNumber(0));
			const balance = await getERC20BalanceFromChain(_currentChain.chainId, fee.token, _userAddress, wrapperToUse);
			if ( balance.balance.lt(amountToCheck) ) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${fee.token}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
					]
				});
				throw new Error();
			}
			if ( !balance.allowance || balance.allowance.amount.lt(amountToCheck) ) {
				try {
					if ( isMultisig ) {
						await makeERC20AllowanceMultisig(_web3, fee.token, _userAddress, amountToCheck, wrapperToUse);
					} else {
						await makeERC20Allowance(_web3, fee.token, _userAddress, amountToCheck, wrapperToUse);
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${fee.token}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approvewnftcollateralfees',
			text: `Approving WNFT collateral fees`,
			status: _AdvancedLoadingStatus.complete
		});
	}

	const getSubmitParamFees = () => {
		if ( inputTransferFeeAddress === '' ) { return []; }
		if ( inputTransferFeeAmount === '' ) { return []; }

		return [{ token: inputTransferFeeAddress, value: new BigNumber(inputTransferFeeAmount) }];
	}
	const getSubmitParamLocks = () => {
		const daysToAdd = parseInt(inputTimeLockDays) || 0;
		if ( daysToAdd > 0 ) {
			return [{ lockType: LockType.time, param: dateToUnixtime(addDays(daysToAdd)) }]
		}
		return [];
	}
	const getSubmitParamRoyalties = () => {
		return royaltyRecipients.map((item) => {
			return { address: item.address || '0x0000000000000000000000000000000000000000', percent: new BigNumber(item.percent) }
		})
	}

	const saftSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		createAdvancedLoaderSAFT(_currentChain, _web3, _userAddress);

		try { await approveOriginalTokens(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
		try { await approve20Collateral(_currentChain, _web3, _userAddress, isMultisig);   } catch(ignored) { return; }
		try { await approve721Collateral(_currentChain, _web3, _userAddress, isMultisig);  } catch(ignored) { return; }
		try { await approve1155Collateral(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }
		try { await approveWNFTFees(_currentChain, _web3, _userAddress, isMultisig);       } catch(ignored) { return; }

		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.loading
		});

		const saftArgs = {
			originalAddress       : {
				assetType: wrapWith !== 'empty' ? _AssetType.ERC721 : _AssetType.empty,
				contractAddress: inputOriginalTokenAddress,
			},
			unwrapDestination: '0x0000000000000000000000000000000000000000',
			fees                  : getSubmitParamFees(),
			locks                 : getSubmitParamLocks(),
			royalties             : getSubmitParamRoyalties(),
			rules                 : inputRules,
			outType               : inputOutAssetType,
			outBalance            : inputCopies,
			recipients            : WNFTRecipients,
			collaterals           : collaterals,
		}
		console.log('saftArgs', saftArgs);

		let txResp;

		if ( userStorageToWrap ) {

			if ( !userWrapper ) { return; }

			try {
				if ( isMultisig ) {
					txResp = await saftTokenOnUserContractMultisig(
						_web3,
						userWrapper,
						userWrapper,
						userStorageToWrap,
						saftArgs,
					);
				} else {
					txResp = await saftTokenOnUserContract(
						_web3,
						userWrapper,
						userWrapper,
						userStorageToWrap,
						saftArgs,
					);
				}
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot wrap token`,
					details: [
						`Original token ${inputOriginalTokenAddress}`,
						`User address: ${_userAddress}`,
						`Args: ${JSON.stringify(saftArgs)}`,
						'',
						e.message || e,
					]
				});
				return;
			}

		} else {

			try {
				if ( isMultisig ) {
					txResp = await saftTokenMultisig(
						_web3,
						batchWorkerContract,
						wrapperContract,
						saftArgs,
					);
				} else {
					txResp = await saftToken(
						_web3,
						batchWorkerContract,
						wrapperContract,
						saftArgs,
					);
				}
			} catch(e: any) {
				setModal({
					type: _ModalTypes.error,
					title: `Cannot wrap token`,
					details: [
						`Original token ${inputOriginalTokenAddress}`,
						`User address: ${_userAddress}`,
						`Args: ${JSON.stringify(saftArgs)}`,
						'',
						e.message || e,
					]
				});
				return;
			}

		}

		updateStepAdvancedLoader({
			id: 'wrap',
			status: _AdvancedLoadingStatus.complete
		});
		unsetModal();
		updateTicket();

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setInputOriginalTokenAddress('');
						setInputTimeLockDays('');
						setInputOutAssetType(_AssetType.ERC721);
						setInputCopies(2);
						setInputRules(decodeRules('0000'));
						setInputTransferFeeAddress('');
						setInputTransferFeeAmount('');
						setInputRoyaltyRecipientAddress('');
						setInputAddWNFTChecked(false);
						setCollaterals([]);
						setInputCollateralAddress('');
						setInputCollateralAmount('');
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Tokens successfully wrapped`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setInputOriginalTokenAddress('');
						setInputTimeLockDays('');
						setInputOutAssetType(_AssetType.ERC721);
						setInputCopies(2);
						setInputRules(decodeRules('0000'));
						setInputTransferFeeAddress('');
						setInputTransferFeeAmount('');
						setInputRoyaltyRecipientAddress('');
						setInputAddWNFTChecked(false);
						setCollaterals([]);
						setInputCollateralAddress('');
						setInputCollateralAmount('');
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}
	}

	const getSubmitBtn = () => {
		return (
			<button
				className="btn btn-lg w-100"
				disabled={ !!hasErrors().length }
				onClick={async () => {
					if ( !currentChain ) { return; }
					let _web3 = web3;
					let _userAddress = userAddress;
					let _currentChain = currentChain;

					if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
						setLoading('Waiting for wallet');

						try {
							const web3Params = await getWeb3Force(_currentChain.chainId);

							_web3 = web3Params.web3;
							_userAddress = web3Params.userAddress;
							_currentChain = web3Params.chain;
							unsetModal();
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: 'Error with wallet',
								details: [
									e.message || e
								]
							});
							return;
						}
					}

					if ( !_web3 || !_userAddress || !_currentChain ) { return; }
					const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
					if ( isMultisig && wrapWith === 'predefined' ) {
						setInfo('Predefined contracts are not available for multisig wallets yet');
						return;
					}
					saftSubmit(_currentChain, _web3, _userAddress, isMultisig);
				}}
			>Wrap</button>
		)
	}

	const getCurrentPage = () => {
		if ( currentPage === 'mint' ) {
			return ( <MintPage closePageFunction={(address) => {
				setCurrentPage('saft');
				if ( address ) {
					setInputOriginalTokenAddress(address);
					setRequestOriginalTokenAddress(address);
				}
			}} /> )
		}

		if ( currentPage === 'addcoll' ) {
			return (
				<main className="s-main">
				<div className="container">

					<div className="row mb-6 align-items-center">
						{ getSubscriptionBlock() }
						{ getPageSelector() }
					</div>

					<AddCollateralPage
						batchWorkerContract={ batchWorkerContract }
						wrapperContract={ wrapperContract }
						techToken={ techToken }
						defaultStorages={defaultStorages}

						whitelistContract={ whitelistContract }
						enabledForFee={ enabledForFee }
						enabledForCollateral={ enabledForCollateral }
						enabledRemoveFromCollateral={ enabledRemoveFromCollateral }
						maxCollaterals={ maxCollaterals }
					/>

				</div>
				</main>
			)
		}

		return (
			<main className="s-main">
			<div className="container">

				<div className="row mb-6 align-items-center">
					{ getSubscriptionBlock() }
					{ getPageSelector() }
				</div>

				<div className="wf-settings mb-8">
					<div className="row">
						{ getModeSelector() }
					</div>
				</div>

				{ getContractsBlock() }
				{ getOriginalContractBlock() }

				{ getStandartSelectorBlock() }
				{ getAdvancedOptionsBlock() }

				{ getFeeBlock() }
				{ getRoyaltyBlock() }

				{ getWNFTRecipientsBlock() }

				{ getCollateralBlock() }

				<div className="row mb-7 mb-lg-9 justify-content-sm-between">
					<div className="col-12 col-sm-3 mt-3 order-2 order-sm-1">
						{ getClearBtn() }
					</div>
					<div className="col-12 col-sm-6 mt-3 order-1 order-sm-2">
						{ getSubmitBtn() }
						{ getErrorsBlock() }
					</div>
				</div>

			</div>
			{ getCustomContractModal() }
			</main>
		)
	}

	return getCurrentPage();

}