
import React, { useContext, useEffect, useState } from 'react';
import Dropzone from 'react-dropzone';

import {
	fetchSwarmStamp,
	getOracleNftMinterSign
} from '../../models/mint';

import axios from "axios";

import config from '../../app.config.json';

import icon_i_del            from '../../static/pics/i-del.svg';
import icon_loader           from '../../static/pics/loader-orange.svg';

import NumberInput from '../NumberInput';
import {
	BigNumber,
	_AssetType
} from '@envelop/envelop-client-core';
import {
	InfoModalContext,
	Web3Context,
	_ModalTypes
} from '../../dispatchers';
import TippyWrapper from '../TippyWrapper';
import { NftMinterContract } from '../../models/mint';

type MintPageProps = {
	closePageFunction? : (address?: string) => void;
}

export default function MintPage(props: MintPageProps) {

	const scrollable: any = {};
	const {
		closePageFunction
	} = props;

	const metaPinataApi = config.PINATA_AUTH;

	const {
		userAddress,
		currentChain,
		web3,
		currentChainId
	} = useContext(Web3Context);
	const {
		setModal,
		unsetModal,
	} = useContext(InfoModalContext);

	const [ minter721, setminter721 ] = useState('');
	const [ minter1155, setminter1155 ] = useState('');
	const [ nftMinterContract, setnftMinterContract ] = useState<NftMinterContract | undefined>(undefined);

	const [ fileReader, setfileReader ] = useState<FileReader>(new FileReader());
	const [ nftImagePreview, setnftImagePreview ] = useState<string | ArrayBuffer>('');
	const [ nftImageMimeType, setnftImageMimeType ] = useState('');
	const [ inputNftImageUrl, setinputNftImageUrl ] = useState('');
	const [ inputNftInfoName, setinputNftInfoName ] = useState('');
	const [ inputNftInfoDesc, setinputNftInfoDesc ] = useState('');
	const [ descriptionRows, setdescriptionRows ] = useState(5);
	const [ nftPropertiesData, setnftPropertiesData ] = useState<Array<{
		type: string,
		name: string
	}>>([]);
	const [ nftPropertiesDataTmp, setnftPropertiesDataTmp ] = useState<Array<{
		type: string,
		name: string
	}>>([]);
	const [ uploadedNftImageUrl, setuploadedNftImageUrl ] = useState('');
	const [ uploadedNftJsonUrl, setuploadedNftJsonUrl ] = useState('');
	const [ mintedNftTxId, setmintedNftTxId ] = useState('');
	const [ mintedNftContract, setmintedNftContract ] = useState('');
	const [ imageModalOpened, setimageModalOpened ] = useState(false);
	const [ nftPropertiesOpened, setnftPropertiesOpened ] = useState(false);
	const [ advancedOptionsOpened, setadvancedOptionsOpened ] = useState(false);
	const [ savingMetadataOpened, setsavingMetadataOpened ] = useState(false);
	const [ inputStandart, setinputStandart ] = useState(_AssetType.ERC721);
	const [ inputBatch, setinputBatch ] = useState('1')  ;
	const [ inputCopies, setinputCopies ] = useState('2')  ;
	const [ metaHostingType, setmetaHostingType ] = useState(2) ;
	const [ currentContract, setcurrentContract ] = useState('');

	const [ showErrors, setshowErrors ] = useState<Array<string>>([]);

	useEffect(() => {

		if ( !web3 ) { return; }

		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChainId });
		if ( foundChain && foundChain.nftMinterContract721 && foundChain.nftMinterContract1155 ) {
			console.log('foundChain.nftMinterContract721', foundChain.nftMinterContract721);
			setminter721(foundChain.nftMinterContract721);
			console.log('foundChain.nftMinterContract1155', foundChain.nftMinterContract1155);
			setminter1155(foundChain.nftMinterContract1155);
			setnftMinterContract(new NftMinterContract({
				chainId                 : currentChainId,
				web3                    : web3,
				contract721Address      : foundChain.nftMinterContract721,
				contract1155Address     : foundChain.nftMinterContract1155,
				userAddress             : userAddress || '',
			}));
		}
	}, [ currentChainId ])

	const getGeneralInfoBlock = () => {

		const showError = showErrors.find((item) => { return item === 'nftimg' });
		return (
			<div
				className="c-wrap"
				ref={ (e) => { scrollable.generalInfo = e } }
			>
				<div className="row">
					<div className="col-md-5 mb-5 mb-md-0">
						{ getNftImgPreview() }
						<div className="input-group">
							<label className="input-label">
								or enter an external URL
								<TippyWrapper
									msg="Paste full URL to the file from external source"
								>
									<span className="i-tip ml-1"></span>
								</TippyWrapper>
								<a
									href="#"
									className="text-green ml-2"
									onClick={(e) => {
										setimageModalOpened(true)
									}}
									>Where do I get images?</a>
							</label>
							<input
								className={`input-control ${ showError ? 'has-error' : '' }`}
								type="text"
								value={ inputNftImageUrl }
								onChange={(e) => {
									getNftImageUrl(e.target.value)

									setinputNftImageUrl(e.target.value);
									setshowErrors(showErrors.filter((item) => { return item !== 'emptyInfoUrl' }));

								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented) { return; }
								}}
								disabled={ ((nftImagePreview && !inputNftImageUrl) ? true : false) }
							/>
						</div>
						<div>
							<div className="mb-2"> <small className="text-muted">File types supported: JPG, PNG, GIF, SVG, MP4, WEBM, MP3, WAV, OGG, GLB, GLTF. </small></div>
							<div> <small className="text-muted">Max size: 10 MB</small></div>
						</div>
					</div>
					<div className="col-md-7 pl-md-6">
						<div className="input-group">
							<label className="input-label">Name<sup className="text-red">*</sup></label>
							<input
								className={`input-control ${ showError ? 'has-error' : '' }`}
								type="text"
								value={ inputNftInfoName }
								onChange={(e) => {
									setinputNftInfoName(e.target.value);
									setshowErrors(showErrors.filter((item) => { return item !== 'emptyInfoName' }));
								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented) { return; }
								}}
							/>
						</div>
						<div className="input-group mb-6">
							<label className="input-label">Description<sup className="text-red">*</sup></label>
							<textarea
								className="input-control"
								rows={ descriptionRows }
								value={ inputNftInfoDesc }
								onChange={(e) => {
									setinputNftInfoDesc(e.target.value);
									setshowErrors(showErrors.filter((item) => { return item !== 'emptyInfoDesc' }));
								}}
								onKeyPress={(e) => {
									if ( e.defaultPrevented) { return; }
								}}
							></textarea>
						</div>
						{ getPropertiesBlock() }
					</div>
				</div>
			</div>
		)
	}
	const getNftImageUrl = async (url: string) => {
		if(!url.toString().match(/http/i) || !url) {
			setnftImagePreview('')
		}
		else {
			const processResponse = (res:any) => {
				const statusCode = res.status
				const contentType = res.headers.get("Content-Type")
				const data = res.arrayBuffer()
				return Promise.all([statusCode, contentType, data]).then(res => ({
					statusCode: res[0],
					contentType: res[1],
					data: res[2]
				}))
			}
			await fetch( url )
				.then(processResponse)
				.then( res => {
					const { statusCode, contentType, data } = res
					if(statusCode === 200) {
						const blob = new Blob( [data] )
						const file = fileReader
						file.addEventListener('loadend', async (e) => {
							const result = e.target?.result
							if (result) {
								setnftImagePreview(result)
								setnftImageMimeType(contentType)
								setinputNftImageUrl(url)
							}
						})
						file.readAsDataURL(blob)
					}
					else {
						setnftImagePreview('')
					}
				})
				.catch( error => {
					console.error(error)
				})
		}
	}
	const getNftImgPreview = () => {

		if ( !nftImagePreview ) {
			return (
				<Dropzone
					onDrop={(files) => { previewOnDrop(files) }}
					accept={{
						'image/*': [],
						'audio/*': [],
						'video/*': [],
						'model/*': [],
					}}
					noKeyboard={ true }
				>
					{
						({ getRootProps, getInputProps, isDragActive, open }) => (
							<div {...getRootProps({ className: `upload-container nft-upload mb-4 dropzone ${isDragActive ? 'file-dragged' : ''}` })}>
								<div className="upload-poopover">
									<div className="inner">
										<div className="h5 mb-0">
											<input {...getInputProps()} />
											<p>Drop your file here <br /> or click to browse</p>
										</div>
									</div>
								</div>
							</div>
						)
					}
				</Dropzone>
			)
		}

		return  (
			<div className="nft-upload-img">
				{ showNftImgPreview() }
				<button
					className="btn-del"
					onClick={() => {
						setnftImagePreview('')
						setinputNftImageUrl('')
					}}
				><img src={ icon_i_del} alt="" /></button>
			</div>
		)
	}
	const showNftImgPreview = () => {
		const nftImage = nftImagePreview ? nftImagePreview.toString() : ''
		if ( nftImageMimeType.includes("video/") ) {
			return (
				<video className="img" style={{ width: "100%", height: "100%" }} loop={ true } autoPlay={ true } muted={ true }>
					<source src={ nftImage } type={ nftImageMimeType } />
				</video>
			)
		}
		else if ( nftImageMimeType.includes("audio/") ) {
			return (
				<audio controls={ true } preload="none">
				    <source src={ nftImage } type={ nftImageMimeType } />
				</audio>
			)
		}
		else {
			return (
				<img
					src={ nftImage }
					className="img"
					alt=""
				/>
			)
		}
	}
	const previewOnDrop = (files: Array<File>) => {
		if(files[0]) {
			const imageMimeType = /(image|audio|video)\/(png|jpg|jpeg|svg|gif|webp|webm|mpeg|mp3|mp4|wav|ogg|glb|gltf)/i
			const contentType = files[0].type
			if (!contentType.match(imageMimeType)) {
				console.log("Unsupported MimeType file - " + contentType);
				return;
			}
			const file = fileReader
			file.addEventListener('loadend', async (e) => {
				const result = e.target?.result
				if (result) {
					setnftImagePreview(result)
					setnftImageMimeType(contentType)
					setinputNftImageUrl('')
				}
			})
			file.readAsDataURL(files[0])
		}
	}
	const getPropertiesBlock = () => {

		if(!nftPropertiesDataTmp.length) {
			addNftEmptyProperty()
		}

		return (
			<div className="input-group mb-0">
				<button
					className="btn btn-outline"
					onClick={(e) => {
						setnftPropertiesDataTmp(nftPropertiesData)
						setnftPropertiesOpened(true)
					}}
				>
					Add properties
				</button>
				{ nftPropertiesData.length ? (
					<>
						<div className="input-label mt-6">NFT Properties</div>
						<div className="c-wrap__table with-del-btn mt-3">
						{
							nftPropertiesData?.map((data, index)=>{
								const {type, name} = data
								return (
									<div
										key={index}
										className="item"
									>
										<div className="row">
											<div className="col-12 col-sm-5 mb-2">
												<span className="text-break text-muted">{type}</span>
											</div>
											<div className="col-12 col-sm-7 mb-2">
												<span className="text-break">{name}</span>
											</div>
											<button
												className="btn-del"
												onClick={() => { deleteNftProperties(index,false) }}
											><img src={ icon_i_del } alt="" /></button>
										</div>
									</div>
								)
							})
						}
						</div>
					</>
				) : null }
			</div>
		)
	}
	const showImageModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setimageModalOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setimageModalOpened(false)
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">Where do I get images?</div>
								<ol>
									<li className="mb-4"><strong>Unsplash</strong> is one of the most popular websites where you can find a vast collection of high-quality photos. There are many images related to cryptocurrency, such as Bitcoin and blockchain, available on Unsplash.</li>
									<li className="mb-4"><strong>Pixabay</strong> is another popular website where you can find free photos, vector images, and videos. There are also many images related to cryptocurrency available on Pixabay.</li>
									<li className="mb-4"><strong>Pexels</strong> is another website where you can find free images and videos. There are several images related to cryptocurrency available on Pexels.</li>
									<li className="mb-4"><strong>Freepik</strong> is a website where you can find free and premium images, vector images, and PSD files. There are several images related to cryptocurrency available on Freepik.</li>
									<li><strong>Flaticon</strong> is another website where you can find free and premium icons related to cryptocurrency. There are many icons related to Bitcoin, blockchain, and more available on Flaticon.</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const showNftProperties = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
						e.stopPropagation()
						if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
							setnftPropertiesOpened(false)
						}
					}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setnftPropertiesOpened(false)
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">NFT Properties</div>
								<div className="nft-property__form">
									<div className="row d-none d-sm-flex">
										<div className="col-sm-5">
											<div className="input-group">
												<label className="input-label">Trait
													<TippyWrapper
														msg="Fill in name of trait that you want save in metadata of NFT"
													>
														<span className="i-tip ml-1"></span>
													</TippyWrapper>
												</label>
											</div>
										</div>
										<div className="col-sm-5">
											<div className="input-group">
												<label className="input-label">Value
													<TippyWrapper
														msg="Fill in value of trait that you want save in metadata of NFT"
													>
														<span className="i-tip ml-1"></span>
													</TippyWrapper>
												</label>
											</div>
										</div>
									</div>
									{
										nftPropertiesDataTmp?.map((data, index)=>{
											const {type, name} = data
											return (
												<div
													key={index}
													className="row mb-4 mb-sm-0"
												>
													<div className="col-sm-5">
														<div className="input-group">
															<label className="input-label d-sm-none">Trait </label>
															<input
																type="text"
																className="input-control"
																value={type}
																name="type"
																onChange={(e) => { changeNftProperties(index,e) }}
															/>
														</div>
													</div>
													<div className="col-sm-5">
														<div className="input-group">
															<label className="input-label d-sm-none">Value </label>
															<input
																type="text"
																className="input-control"
																value={name}
																name="name"
																onChange={(e) => { changeNftProperties(index,e) }}
															/>
														</div>
													</div>
													<div className="col-sm-2 col-del">
														<button
															className="btn-link px-0 btn-sm"
															onClick={() => { deleteNftProperties(index,true) }}
															> Delete</button>
													</div>
												</div>
											)
										})
									}
									<button
										className="btn btn-outline btn-sm w-auto"
										onClick={() => { addNftEmptyProperty() }}
									>Add more </button>
								</div>
								<div className="row mt-6">
									<div className="col-sm-5">
										<button
											className="btn w-100"
											onClick={() => {
												setnftPropertiesData(nftPropertiesDataTmp);
												setnftPropertiesOpened(false)
											}}
										>Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const addNftEmptyProperty = () => {
		setnftPropertiesDataTmp([...nftPropertiesDataTmp, { type: '', name: '' }])
	}
	const deleteNftProperties = (index: number, tmp: boolean) => {
		if (tmp) {
			const data = [...nftPropertiesDataTmp]
			data.splice(index, 1)
			setnftPropertiesDataTmp(data)
		}
		else {
			const data = [...nftPropertiesData]
			data.splice(index, 1)
			setnftPropertiesData(data)
		}
	}
	const changeNftProperties = (i: number, e: React.ChangeEvent<HTMLInputElement>) => {
		const {name, value} = e.target
		const data = [...nftPropertiesDataTmp]
		data[i][name as keyof typeof data[0]] = value
		setnftPropertiesDataTmp(data)
	}
	const getAdvancedOptionsBlock = () => {
		return (
			<div className="c-wrap">
				<div
					className={`c-wrap__toggle ${advancedOptionsOpened ? 'active' : ''}`}
					onClick={() => {
						setadvancedOptionsOpened(!advancedOptionsOpened)
					}}
				>
					<div><b>Advanced options</b></div>
				</div>
				<div className="c-wrap__dropdown">
					<div className="input-group pt-5 mb-0">
						<label className="input-label pb-1">NFT metadata hosting place:</label>
						<div className="mb-3">
							<label className="checkbox">
								<input
									type="radio"
									name="meta-hosting"
									value="2"
									checked={ metaHostingType === 2 }
									onChange={() => { setmetaHostingType(2) }}
								/>
								<span className="check"> </span>
								<span className="check-text">IPFS
									<TippyWrapper
										msg="Use IPFS decentralised data storage"
									>
										<span className="i-tip ml-1"></span>
									</TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input
									type="radio"
									name="meta-hosting"
									value="1"
									checked={ metaHostingType === 1 }
									onChange={() => { setmetaHostingType(1) }}
								/>
								<span className="check"> </span>
								<span className="check-text">Swarm
									<TippyWrapper
										msg="Use Swarm decentralised data storage"
									>
										<span className="i-tip ml-1"></span>
									</TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input type="radio" name="meta-hosting" disabled />
								<span className="check"> </span>
								<span className="check-text">Envelop
									<TippyWrapper
										msg="Work on support is in progress"
									>
										<span className="i-tip ml-1"></span>
									</TippyWrapper>
								</span>
							</label>
						</div>
						<div className="mb-3">
							<label className="checkbox">
								<input type="radio" name="meta-hosting" disabled />
								<span className="check"> </span>
								<span className="check-text">Unstopable NFT
									<TippyWrapper
										msg="Work on support is in progress"
									>
										<span className="i-tip ml-1"></span>
									</TippyWrapper>
								</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const getCopiesBlock = () => {
		const showError = showErrors.find((item) => { return item === 'inputCopies' });
		if ( inputStandart === _AssetType.ERC1155 ) {
			return (
				<div className="col-12 col-lg-3">

					<div className="input-group mb-md-0">
						<label className="input-label">Quantity</label>
						<NumberInput
							value={ parseInt(inputCopies) || undefined }
							placeholder='2'
							onChange={(e: number | undefined) => {
								setinputCopies(`${e || 2}`);
								setshowErrors(showErrors.filter((item) => { return item !== 'inputCopies' }));
							}}
							min={ 1 }
							inputClass={ showError ? 'has-error' : '' }
						/>
					</div>
				</div>
			)
		}
	}
	const getBatchBlock = () => {
		const showError = showErrors.find((item) => { return item === 'inputBatch' });
		return (
			<div className="col-12 col-lg-4">

				<div className="input-group mb-md-0">
					<label className="input-label">Batch
						<TippyWrapper
							msg="Set amount of NFTs that you want to mint in batch"
						>
							<span className="i-tip ml-1"></span>
						</TippyWrapper>
					</label>
						<NumberInput
							value={ parseInt(inputBatch) || undefined }
							placeholder='2'
							onChange={(e: number | undefined) => {
								setinputBatch(`${e || 1}`);
								setshowErrors(showErrors.filter((item) => { return item !== 'inputBatch' }));
							}}
							min={ 1 }
							inputClass={ showError ? 'has-error' : '' }
						/>
				</div>
			</div>
		)
	}
	const getStandartSelectorBlock = () => {

		const eipPrefix = currentChain?.EIPPrefix ? currentChain.EIPPrefix : 'ERC'

		return (
			<div
				className="c-wrap"
				ref={ (e) => { scrollable.standart = e } }
			>
				<div className="row">
					<div className="col-12 col-lg-5">
						<div className="input-group mb-md-0">
							<label className="input-label">NFT Standard
								<TippyWrapper
									msg="Check type of NFTs that you want mint"
								>
									<span className="i-tip ml-1"></span>
								</TippyWrapper>
							</label>
							<div className="row row-sm">
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="nft-standard"
											value={ _AssetType.ERC721 }
											checked={ inputStandart === _AssetType.ERC721 }
											onChange={() => {
												setinputStandart(_AssetType.ERC721);
												setcurrentContract(minter721);
											}}
										/>
										<span className="check"> </span>
										<span className="check-text"><b>{ eipPrefix }-721</b></span>
									</label>
								</div>
								<div className="col-auto my-2">
									<label className="checkbox">
										<input
											type="radio"
											name="nft-standard"
											value={ _AssetType.ERC1155 }
											checked={ inputStandart === _AssetType.ERC1155 }
											onChange={() => {
												setinputStandart(_AssetType.ERC1155);
												setcurrentContract(minter1155);
											}}
										/>
										<span className="check"> </span>
										<span className="check-text"> <b>{ eipPrefix }-1155</b></span>
									</label>
								</div>
							</div>
						</div>
					</div>
					{ getCopiesBlock() }
					{ getBatchBlock() }
				</div>
			</div>
		)
	}
	const savingMetadataModal = () => {
		return (
			<div className="modal">
				<div className="modal__inner" onClick={ (e) => {
					e.stopPropagation()
					if ((e.target as HTMLTextAreaElement).className === 'modal__inner') {
						setsavingMetadataOpened(false);
					}
				}}>
					<div className="modal__bg"></div>
					<div className="container">
						<div className="modal__content">
							<div
								className="modal__close"
								onClick={() => {
									setsavingMetadataOpened(false);
								}}
							>
								<svg width="37" height="37" viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fillRule="evenodd" clipRule="evenodd" d="M35.9062 36.3802L0.69954 1.17351L1.25342 0.619629L36.4601 35.8263L35.9062 36.3802Z" fill="white"></path>
									<path fillRule="evenodd" clipRule="evenodd" d="M0.699257 36.3802L35.9059 1.17351L35.3521 0.619629L0.145379 35.8263L0.699257 36.3802Z" fill="white"></path>
								</svg>
							</div>
							<div className="modal__header">
								<div className="h2">Minting your NFT</div>
							</div>
							<div className="c-approve">
								<div className={`c-approve__step ${ uploadedNftImageUrl ? 'in-queue' : 'active' }`}>
									<div className="row">
										<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Saving your metadata<span className="dots">...</span></span> </div>
										<div className="col-12 col-sm-auto order-1 order-sm-1">
											<div className="status"><img className={ !uploadedNftImageUrl ? 'loader' : '' } src={ icon_loader } alt="" /></div>
										</div>
									</div>
								</div>
								{ savingMetadataQueue() }
								{ mintingStatusQueue() }
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
	const savingMetadataQueue = () => {
		if(uploadedNftImageUrl && uploadedNftJsonUrl) {
			const nft_txid = mintedNftTxId
			return(
							<div className={`c-approve__step ${ nft_txid ? 'in-queue' : 'active' }`}>
								<div className="row">
									<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Minting NFT for you<span className="dots">...</span></span> </div>
									<div className="col-12 col-sm-auto order-1 order-sm-1">
										<div className="status"><img className={ !nft_txid ? 'loader' : '' } src={ icon_loader } alt="" /></div>
									</div>
								</div>
							</div>
			)
		}
	}
	const mintingStatusQueue = () => {
		if(uploadedNftJsonUrl && mintedNftTxId) {
			const nft_txid = mintedNftTxId
			return(
							<div className="c-approve__step active">
								<div className="row">
									<div className="col-12 col-sm-auto order-2 order-sm-1"><span className="ml-2">Minted with transaction ID: <a href={ `${currentChain?.explorerBaseUrl || ''}/tx/` + nft_txid } target="_blank" rel="noreferrer">{ shortenHash(nft_txid) }</a></span> </div>
									<div className="col-12 col-sm-auto order-1 order-sm-1">
										<div className="status"><img className={ !nft_txid ? 'loader' : '' } src={ icon_loader } alt="" /></div>
									</div>
								</div>
							</div>
			)
		}
	}
	const shortenHash = (hash: any) => {
		return hash.substring(0, 4) + "..." + hash.substring(hash.length - 4, hash.length)
	}
	const hasErrors = () => {

		const errors: Array<{ msg: string, block: HTMLElement, showError?: string }> = [];

		if (!nftImagePreview) {
			if (!inputNftImageUrl) {
				errors.push({ msg: 'Add a local file  or an external URL', block: scrollable.generalInfo })
			}
			else {
				errors.push({ msg: 'Wrong URL', block: scrollable.generalInfo })
			}
		}

		if (!inputNftInfoName) {
			errors.push({ msg: 'Add NFT name', block: scrollable.generalInfo })
		}

		if (!inputNftInfoDesc) {
			errors.push({ msg: 'Add NFT description', block: scrollable.generalInfo })
		}

		if (isNaN(+inputBatch) || +inputBatch < 1) {
			errors.push({ msg: 'Correct batch amount value', block: scrollable.standart })
		}

		if (isNaN(+inputCopies) || +inputCopies < 1) {
			errors.push({ msg: 'Correct supply amount value', block: scrollable.standart })
		}

		return errors;
	}
	const getErrorsBlock = () => {
		const errors = hasErrors();

		if ( !errors.length ) { return null; }

		return (
			<div className="c-errors mb-3">
				<div className="mb-3">To enable Mint button, correct the following errors:</div>
				<ul>
				{
					errors.map((item) => {
						return (
							<li key={ item.msg }>
								<button
									className="btn-link"
									onClick={() => {
										item.block.scrollIntoView();
										if ( item.showError ) {
											setshowErrors([
												...showErrors.filter((iitem) => { return iitem !== item.showError }),
												item.showError
											])
										}
									}}
								>
									{ item.msg }
								</button>
							</li>
						)
					})
				}
				</ul>
			</div>
		)
	}
	const getCreateBtn = () => {
		return (
			<button
				className="btn btn-lg w-100"
				disabled={
					!!hasErrors().length
				}
				onClick={() => { createSubmit(); }}
			>Mint</button>
		)
	}
	const createSubmit = async () => {
		if ( !nftMinterContract ) { return; }

		setsavingMetadataOpened(true);
		// unset txid if any previous stored
		setmintedNftTxId('');

		let jsonHash: any;

		// process submit to Swarm
		if (metaHostingType === 1) {
			// Save data with Swarm
			const swarmData = await fetchSwarmStamp({
				name: inputNftInfoName,
				desc: inputNftInfoDesc,
				image: nftImagePreview.toString(),
				mime: nftImageMimeType,
				props: nftPropertiesData
			});

			if ( swarmData !== undefined ) {
				if ('image' in swarmData) {
					if (typeof swarmData['image'] === "string") {
						setuploadedNftImageUrl(swarmData['image']);
					}
				}
				if ('json' in swarmData) {
					if (typeof swarmData['json'] === "string") {
						const swarmHash = `bzz://${swarmData['json']}`;
						setuploadedNftJsonUrl(swarmHash);
					}
				}
			} else {
				console.log("Error with saving metadata to Swarm");
				setModal({
					type: _ModalTypes.error,
					title: `There was an error with saving metadata to Swarm. Please try again or choose another metadata hosting option.`,
					buttons: [
						{
							text: 'Close',
							clickFunc: async () => {
								// unsetFormValues();
								unsetModal();
							}
						},
					],
					links: undefined
				});
			}
		}
		// process submit to IPFS
		else if (metaHostingType === 2) {

			const formData = new FormData();
			const file = DataURIToBlob(nftImagePreview.toString());
			formData.append('file', file, inputNftInfoName);

			const metadata = JSON.stringify({
				name: inputNftInfoName + " image",
			});
			formData.append('pinataMetadata', metadata);

			const options = JSON.stringify({
				cidVersion: 0,
			});
			formData.append('pinataOptions', options);

			const url = "https://api.pinata.cloud/pinning/";
			const JWT = `Bearer ${metaPinataApi}`;

			try {
				// post image to IPFS
				const res = await axios.post(url + 'pinFileToIPFS', formData, {
					maxBodyLength: -1,
					headers: {
						'Content-Type': 'multipart/form-data',
						Authorization: JWT
					}
				});
				console.log(res.data);

				if ('IpfsHash' in res.data) {
					const imageHash = `ipfs://${res.data.IpfsHash}`;
					setuploadedNftImageUrl(imageHash);

					let jsonData = JSON.stringify({
						"pinataOptions": {
							"cidVersion": 1
						},
						"pinataMetadata": {
							"name": inputNftInfoName + " JSON"
						},
						"pinataContent": {
							"name": inputNftInfoName,
							"description": inputNftInfoDesc,
							"image": (!inputNftImageUrl) ? imageHash : inputNftImageUrl,
							"attributes": nftPropertiesData.map((item, index) => ({trait_type: item.type, value: item.name}))
						}
					});
					console.log(jsonData);

					try {
						// post JSON to IPFS
						const resJson = await axios.post(url + 'pinJsonToIPFS', jsonData, {
							headers: {
								'Content-Type': 'application/json',
								Authorization: JWT
							}
						});
						console.log(resJson.data);

						if ('IpfsHash' in resJson.data) {
							jsonHash = `ipfs://${resJson.data.IpfsHash}`;
							setuploadedNftJsonUrl(jsonHash);
							console.log('https://gateway.pinata.cloud/ipfs/' + resJson.data.IpfsHash);
						}

					} catch (error) {
						console.log(error);
						setModal({
							type: _ModalTypes.error,
							title: `There was an error with saving metadata to IPFS. Please try again or choose another metadata hosting option.`,
							buttons: [
								{
									text: 'Close',
									clickFunc: async () => {
										// unsetFormValues();
										unsetModal();
									}
								},
							],
							links: undefined
						});
					}

				}
				else {
					console.log("Error with IPFS image hash");
				}
			} catch (error) {
				console.log(error);
				setModal({
					type: _ModalTypes.error,
					title: `There was an error with saving metadata to IPFS. Please try again or choose another metadata hosting option.`,
					buttons: [
						{
							text: 'Close',
							clickFunc: async () => {
								// unsetFormValues();
								unsetModal();
							}
						},
					],
					links: undefined
				});
			}

		}

		// process with mint
		if (jsonHash) {
			// prepare form data
			const _inputStandart = ( inputStandart === _AssetType.ERC721 ) ? 721 : 1155;
			const _inputBatch: number = ( isNaN(+inputBatch) ) ? 1 : Number(inputBatch);
			const _inputCopies: number = ( isNaN(+inputCopies) ) ? 1 : Number(inputCopies);

			console.log("NFT token URI: " + jsonHash);

			try {

				// get NFT last ID
				await nftMinterContract.getTotalSupply({
					standart: _inputStandart
				}).then( async (lastTokenId: any) => {
					const nextTokenId = Number(lastTokenId) + 1;

					let oracleSignature: any = await getOracleNftMinterSign({
						address: userAddress || '',
						chain_id: currentChainId,
						token_id: nextTokenId,
						token_uri: jsonHash,
						batch: _inputBatch,
						amount: _inputCopies,
						standart: _inputStandart
					});

					if ( !oracleSignature ) {

						setsavingMetadataOpened(false);

						setModal({
							type: _ModalTypes.error,
							title: "Cannot mint token: No oracle signature",
							buttons: undefined,
							links: undefined,
						});
					}
					else {
						const oracle_signature = oracleSignature.oracle_signature;

						// mint that NFT
						nftMinterContract?.mintWithURI({
							tokenId: nextTokenId,
							tokenURI: jsonHash,
							batch: _inputBatch,
							amount: _inputCopies,
							standart: _inputStandart,
							oracleSignature: oracle_signature,
							userAddress: userAddress || '',
						})
						.then((data: any) => {
							if ('transactionHash' in data) {
								console.log("Minted with txid " + data['transactionHash']);

								// get minted token IDs and contract address
								let viewNftTokenId = '';
								// let viewNftLastTokenId = 0;
								let viewNftContract = '';
								let viewNftObject = [];
								let tokenIds = [];

								viewNftObject = ( _inputStandart === 721 ) ? data.events.Transfer : data.events.TransferSingle;

								if( viewNftObject.length > 1 ) {
									viewNftObject.map( (item:any) => tokenIds.push( ( _inputStandart === 721 ) ? item.returnValues.tokenId : item.returnValues.id ) );
									viewNftContract = viewNftObject[0].address;
								}
								else {
									tokenIds.push( ( _inputStandart === 721 ) ? viewNftObject.returnValues.tokenId : viewNftObject.returnValues.id );
									viewNftContract = viewNftObject.address;
								}

								viewNftTokenId = tokenIds.map(i => i).join(', ');
								// viewNftLastTokenId = Number(tokenIds.pop());

								setmintedNftContract(viewNftContract);
								setmintedNftTxId(data.transactionHash);

								setModal({
									type: _ModalTypes.info,
									title: `${ viewNftTokenId.length === 1 ? 'NFT has been minted' : 'NFTs have been minted' }`,
									text: currentChain?.hasOracle ? [
										{ text: `Contract address: ${viewNftContract}` },
										{ text: `Token ids: ${viewNftTokenId}` },
										{ text: `After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page.` },
									] : [
										{ text: `Contract address: ${viewNftContract}` },
										{ text: `Token ids: ${viewNftTokenId}` },
									],
									buttons: [
										{
											text: `Go to dashboard`,
											clickFunc: () => { window.location.href = `/dashboard`; }
										},
										{
											text: 'Close',
											clickFunc: () => { unsetModal() }
										},
									],
									links: [{
										text: `View on ${currentChain?.explorerName || ''}`,
										url: `${currentChain?.explorerBaseUrl || ''}tx/${data.transactionHash}`
									}]
								});

								unsetFormValues();

							}
							else {
								console.log('No TxID in response');
								setModal({
									type: _ModalTypes.error,
									title: 'Cannot mint NFT. Please try again.',
									buttons: [
										{
											text: 'Close',
											clickFunc: async () => {
												unsetFormValues();
												unsetModal();
											}
										},
									],
									links: undefined,
								});
							}
						})
						.catch((e: any) => {
							console.log('Cannot mint NFT', e);
							setModal({
								type: _ModalTypes.error,
								title: 'Cannot mint NFT. Please try again.',
								buttons: [
									{
										text: 'Close',
										clickFunc: async () => {
											unsetFormValues();
											unsetModal();
										}
									},
								],
								links: undefined,
							});
						});
					}
				});
			} catch (error) {
				console.log(error);
				setModal({
					type: _ModalTypes.error,
					title: `There was an error with selected network RPC. Change RPC in your wallet.`,
					buttons: [
						{
							text: 'Close',
							clickFunc: async () => {
								// unsetFormValues();
								unsetModal();
							}
						}
					],
					links: [
						{
							text: 'List of RPC',
							url: 'https://chainlist.org/chain/' + currentChainId
						}
					]
				});
			}
		}
	}
	const DataURIToBlob = (dataURI: string) => {
        const splitDataURI = dataURI.split(',');
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++)
            ia[i] = byteString.charCodeAt(i);

        return new Blob([ia], { type: mimeString });
    }
	const unsetFormValues = () => {
		setsavingMetadataOpened(false);
		setinputNftInfoName('');
		setinputNftInfoDesc('');
		setinputNftImageUrl('');
		setfileReader(new FileReader());
		setnftImagePreview('');
		setnftImageMimeType('');
		setnftPropertiesData([]);
		setinputBatch('1');
		setinputCopies('1');
		setmetaHostingType(2);
		setuploadedNftImageUrl('');
		setuploadedNftJsonUrl('');
	}

	return (
		<>
			<main className="s-main">
				<div className="container">
					<div className="wrap__header">
						<div className="h3 mt-0">Mint NFT</div>
						{
							closePageFunction ? (
								<button
									className="btn btn-outline"
									onClick={() => { if (closePageFunction) { closePageFunction( mintedNftContract ) }}}
								>Back to SAFT</button>
							) : null
						}
						<div className="mt-10">
							<p><small>With this dApp you can mint ERC-721 or ERC-1155 NFT and make it smarter in few clicks in other dApps.</small><br /><small className="text-muted">* This is a MVP version. Please let us know about any problems or errors occured - <a href="mailto:info@envelop.is">info@envelop.is</a></small></p>
						</div>
					</div>
					{ getGeneralInfoBlock() }
					<div className="row">
						<div className="col-md-5 col-lg-5">
							{ getAdvancedOptionsBlock() }
						</div>
						<div className="col-md-7 col-lg-7">
							{ getStandartSelectorBlock() }
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-5 col-lg-5">
							{ getErrorsBlock() }
						</div>
					</div>
					<div className="row">
						<div className="col-12 col-md-7 col-lg-4">
							{ getCreateBtn() }
						</div>
					</div>
				</div>
			</main>
			{ imageModalOpened ? showImageModal() : null }
			{ nftPropertiesOpened ? showNftProperties() : null }
			{ savingMetadataOpened ? savingMetadataModal() : null }
		</>
	)
}