
import React, {
	useContext,
	useEffect,
	useRef,
	useState
} from "react";

import Dropzone from "react-dropzone";

import {
	BigNumber,
	ChainType,
	CollateralItem,
	ERC20Type,
	NFT,
	WNFT,
	Web3,
	_AssetType,
	addThousandSeparator,
	chainTypeToERC20,
	combineURLs,
	compactString,
	getChainId,
	getERC20BalanceFromChain,
	getNullERC20,
	getWNFTById,
	getWNFTWrapperContract,
	localStorageGet,
	makeERC20Allowance,
	makeERC20AllowanceMultisig,
	removeThousandSeparator,
	tokenToFloat,
	tokenToInt
} from "@envelop/envelop-client-core";

import {
	AdvancedLoaderStageType,
	ERC20Context,
	InfoModalContext,
	SubscriptionContext,
	Web3Context,
	_AdvancedLoadingStatus,
	_ModalTypes
} from "../../dispatchers";

import {
	addCollateralBatch,
	addCollateralBatchMultisig
} from "../../models/saft";

import CollateralViewer  from "../CollateralViewer";
import CollateralSummary from "../CollateralSummary";
import CoinSelector      from "../CoinSelector";
import TippyWrapper      from "../TippyWrapper";

import icon_i_del        from '../../static/pics/i-del.svg';
import icon_i_attention  from '../../static/pics/i-attention.svg';
import default_nft       from '../../static/pics/coins/_default_nft.svg';
import icon_loading      from '../../static/pics/loading.svg';

import config            from '../../app.config.json';
import InputWithOptions from "../InputWithOptions";

type RecipientItem = {
	id: string,
	address: string,
	tokenId: string,
}
type LoadedWNFTItem = {
	address: string,
	tokenId: string,
	token: WNFT | undefined,
	trustedWrapper?: string,
	status: string | undefined,
}

type AddCollateralPageProps = {
	batchWorkerContract: string | undefined,
	wrapperContract: string | undefined,
	techToken: string,
	defaultStorages: Array<{ contractAddress: string, tokenId: string, assetType: _AssetType }>,

	whitelistContract: string | undefined,
	enabledForFee: Array<string>,
	enabledForCollateral: Array<string>,
	enabledRemoveFromCollateral: Array<string>,
	maxCollaterals: number,
}

export default function AddCollateralPage(props: AddCollateralPageProps) {

	const {
		batchWorkerContract,
		techToken,
		wrapperContract,
		defaultStorages,
		whitelistContract,
		enabledForFee,
		enabledForCollateral,
		enabledRemoveFromCollateral,
		maxCollaterals,
	} = props;

	let scrollable: any = {
		recipients: useRef(null),
		collaterals: useRef(null),
	};

	const {
		userAddress,
		currentChain,
		currentChainId,
		web3,
		getWeb3Force,
		balanceNative
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token
	} = useContext(ERC20Context);
	const {
		setModal,
		unsetModal,
		setLoading,
		createAdvancedLoader,
		updateStepAdvancedLoader,
	} = useContext(InfoModalContext);
	const {
		subscriptionRemainings,
		updateTicket,
		subscriptionServiceExist,
	} = useContext(SubscriptionContext);

	const [ collaterals,                 setCollaterals                 ] = useState<Array<CollateralItem>>([]);
	const [ inputCollateralAddress,      setInputCollateralAddress      ] = useState('');
	const [ inputCollateralAmount,       setInputCollateralAmount       ] = useState('');

	const [ inputContractAddress,        setInputContractAddress        ] = useState('');
	const [ inputTokenId,                setInputTokenId                ] = useState('');
	const [ recipients,                  setRecipients                  ] = useState<Array<RecipientItem>>([]);

	const [ showErrors,                  setShowErrors                  ] = useState<string[]>([]);

	const [ loadedWNFTs,                 setLoadedWNFTs                 ] = useState<LoadedWNFTItem[]>([]);

	useEffect(() => {
		setRecipients([]);
		setCollaterals([]);
		setInputContractAddress('');
		setInputTokenId('');
		setInputCollateralAddress('');
		setInputCollateralAmount('');
	}, [ currentChain ]);

	useEffect(() => {
		if ( !currentChain ) { return; }

		const NFTToLoad = loadedWNFTs.find((item) => { return item.status === 'pending' });
		if ( !NFTToLoad ) { return; }

		getWNFTById(currentChain.chainId, NFTToLoad.address, NFTToLoad.tokenId)
			.then(async (data) => {
				if ( !data ) {
					throw Error('Cannot load token');
				}
				const tokenWrapper = await getWNFTWrapperContract(currentChainId, NFTToLoad.address);
				setLoadedWNFTs((prevState) => {
					return [
						...prevState.filter((item) => { return item.address.toLowerCase() !== NFTToLoad.address.toLowerCase() || item.tokenId !== NFTToLoad.tokenId }),
						{
							...NFTToLoad,
							token: data,
							status: undefined,
							trustedWrapper: tokenWrapper,
						}
					]
				})
			})
			.catch((e) => {
				setLoadedWNFTs((prevState) => {
					return [
						...prevState.filter((item) => { return item.address.toLowerCase() !== NFTToLoad.address.toLowerCase() || item.tokenId !== NFTToLoad.tokenId }),
						{
							...NFTToLoad,
							status: 'Cannot load token'
						}
					]
				})
			});
	}, [ loadedWNFTs ])

	const parseRangeFromStr = (str: string) => {
		const output: Array<number> = [];

		str.split(',').forEach((item) => {

			const range = item.replace('—', '-').split('-');
			if ( range.length === 1 ) {
				const parsed = parseInt(item);
				if ( !isNaN(parsed) ) { output.push(parsed); }
				return;
			}
			if ( range.length === 2 ) {
				const start = parseInt(range[0]);
				const end = parseInt(range[1]);

				if ( end < start ) { return; }

				[...Array(end + 1 - start).keys()].forEach((iitem) => {
					output.push(start + iitem)
				});

				return;
			}

		});

		return output;
	}

	const filterERC20ContractByPermissions = (permissions: {
		enabledForCollateral?        : boolean,
		enabledForFee?               : boolean,
		enabledRemoveFromCollateral? : boolean,
	}): Array<ERC20Type> => {
		if ( whitelistContract === undefined ) {
			if ( currentChain ) {
				return [
					chainTypeToERC20(currentChain),
					...erc20List
				];
			} else {
				return erc20List;
			}
		}

		// OR LOGIC
		const filteredERC20 = erc20List.filter((item) => {
			if ( permissions.enabledForCollateral        && enabledForCollateral.find(       (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledForFee               && enabledForFee.find(              (iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }
			if ( permissions.enabledRemoveFromCollateral && enabledRemoveFromCollateral.find((iitem => { return item.contractAddress.toLowerCase() === iitem.toLowerCase() })) ) { return true; }

			return false;
		})
		if ( !currentChain ) { return filteredERC20; }

		return [
			chainTypeToERC20(currentChain),
			...filteredERC20
		]
	}
	const subscriptionBlocked = () => {
		if ( batchWorkerContract === '' ) { return true; }

		if ( !subscriptionServiceExist ) { return false; }

		if ( !subscriptionRemainings ) { return true; }
		if ( subscriptionRemainings.countsLeft.gt(0) ) { return false; }

		const now = new BigNumber(new Date().getTime()).dividedToIntegerBy(1000);
		if ( subscriptionRemainings.validUntil.gt(now) ) {
			return false;
		}
	}

	const getRecipientRow = (recipient: RecipientItem, position: number) => {

		const foundToken = loadedWNFTs.find((item) => { return item.address.toLowerCase() === recipient.address.toLowerCase() && item.tokenId === recipient.tokenId });

		const getTokenImage = () => {
			if ( !foundToken ) { return default_nft }

			if ( foundToken.status && foundToken.status !== 'pending' ) { return default_nft }
			if ( foundToken.status && foundToken.status === 'pending' ) { return icon_loading }

			if ( !foundToken.token ) { return default_nft }
			if ( !foundToken.status ) { return foundToken.token.image || default_nft }
		}

		const getTokenError = () => {

			if ( !foundToken ) { return 'Cannot load token' }

			if ( foundToken.status === 'pending' ) { return undefined; }

			if ( foundToken.status ) { return foundToken.status; }

			if ( !foundToken.token ) { return 'Cannot load token'; }

			if ( foundToken.trustedWrapper && wrapperContract ) {
				if ( foundToken.trustedWrapper.toLowerCase() !== wrapperContract.toLowerCase() ) { return 'Unsupported wrapper' }
			}

			if ( foundToken.token.rules.noUnwrap ) { return 'Cannot add collateral due to the rule' }
		}

		let error = getTokenError();

		return (
			<>
			<div className={`item ${error ? 'has-error' : ''}`} key={ recipient.id }>
				<div className="row">
					<div className="col-md-1 mb-2">#{ position }</div>
					<div className="col-9 col-md-2 mb-2">
						<span className="text-break">{ compactString(recipient.address) }</span>
					</div>
					<div className="col-3 col-md-auto col-md-1 mb-2">
						<div className="tb-nft"><img src={ getTokenImage() } /></div>
					</div>
					<div className="col-auto col-md-5"><span className="text-break">ID { recipient.tokenId }</span></div>
					<button
						className="btn-del"
						onClick={(e) => {
							const filteredRecipients = recipients.filter((item) => { return item.id !== recipient.id });
							setRecipients(filteredRecipients)
						}}
					>
						<img src={ icon_i_del } alt="" />
					</button>
				</div>
			</div>
			{
				error ? (
					<div className="item-error">{ error }</div>
				) : null
			}
			</>
		)
	}
	const addRecipientDisabled = () => {

		if ( subscriptionBlocked() ) { return 'Add' }
		if ( inputContractAddress === '' ) { return 'Add' }

		if ( !Web3.utils.isAddress(inputContractAddress) ) {
			return 'Wrong address'
		}

		if ( !inputTokenId ) {
			return 'Empty token id'
		}

		if ( inputTokenId ) {
			const foundAdded = recipients.find((iitem) => {
				if ( !iitem.tokenId ) { return true }
				return iitem.tokenId === inputTokenId;
			});
			if ( foundAdded ) { return 'Already added' }
		}

		return ''
	}

	const recipientsOnDrop = (files: Array<File>) => {

		if ( subscriptionBlocked() ) { return; }

		files.forEach((item) => {
			const fileReader = new FileReader();
			fileReader.addEventListener('load', (e) => {
				let text = e.target?.result;
				if ( typeof(text) !== 'string' ) {
					return;
				}
				if ( !text ) { return; }

				let rows: Array<string> = [];
				if ( text.includes('\r\n') ) {
					rows = text.split('\r\n');
				} else {
					if ( text.includes('\n\r') ) {
						rows = text.split('\n\r');
					} else {
						rows = text.split('\n');
					}
				}

				rows.forEach((iitem, idx) => {
					if ( Web3.utils.isAddress(iitem)
					) {
						// whole string is address
						addRecipientRow(iitem, '', idx);
					} else {
						// two comma separated values
						try {
							const address = iitem.split(';')[0];
							const tokenId = iitem.split(';')[1];

							if ( !Web3.utils.isAddress(address) ) { return null; }

							addRecipientRow(address, tokenId, idx);
						} catch(e) { console.log('Cannot add row from file', iitem) }
					}
				});

			});
			fileReader.readAsText(item)
		})
	}
	const addRecipientRow = (address: string, tokenId: string, idx?: number) => {
		const tokenIds = parseRangeFromStr(tokenId);

		setRecipients((prevState) => {
			return [
				...prevState.filter((item) => {
					const foundTokenIds = tokenIds.find((iitem) => { return `${iitem}` === item.tokenId });
					return item.address.toLowerCase() !== address.toLowerCase() || !foundTokenIds
				}),
				...tokenIds.map((item, iidx) => {
					return {
						id: `${new Date().getTime()}${(idx || 0)*10**6}${iidx*10**3}`,
						address: address,
						tokenId: `${item}`
					}
				})
			]
		});

		setLoadedWNFTs((prevState) => {
			return [
				...prevState,
				...tokenIds
				.filter((item) => {
					return !prevState.find((iitem) => { return address.toLowerCase() === iitem.address.toLowerCase() && `${item}` === iitem.tokenId })
				})
				.map((item) => {
					return {
						address: address,
						tokenId: `${item}`,
						token: undefined,
						status: 'pending',
					}
				})
			]
		});
		setInputContractAddress('');
		setInputTokenId('');

	}
	const getRecipientsBlock = () => {
		return (
			<Dropzone
				onDrop={(files) => { recipientsOnDrop(files) }}
				accept={{ 'text/csv': ['.txt', '.csv'] }}

				noClick={ true }
				noKeyboard={ true }
			>
				{
					({
						getRootProps, getInputProps, isDragActive, open
					}) => (
						<div {...getRootProps({ className: `c-wrap c-b-wrap ${isDragActive && !subscriptionBlocked() ? 'file-dragged' : ''}` })}>

							<div className="upload-poopover">
								<div className="inner">
									<div className="h3">Drop your file here</div>
								</div>
							</div>

							<input {...getInputProps()} />
							<div className="c-wrap__header" ref={ scrollable.recipients }>
								<div className="h3">Collateral Recipients</div>
							</div>
							<div className="c-wrap__form">
								<div className="row">
									<div className="col col-12 col-md-5">
										<label className="input-label">Contract Address</label>
										<InputWithOptions
											placeholder="Paste here"
											value={ inputContractAddress }
											onChange={(e) => {
												const value = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
												setInputContractAddress(value);
											}}
											onSelect={(e) => {
												const value = e.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
												setInputContractAddress(value);
											}}
											options={ defaultStorages.map((item) => { return { label: `${currentChain?.EIPPrefix || 'ERC'}-721 ${compactString(item.contractAddress)}`, value: item.contractAddress } }) }
											showArrow={ true }
										/>
									</div>
									<div className="col col-12 col-md-4">
										<label className="input-label">Tokens ID range</label>
										<input
											className="input-control"
											type="text"
											placeholder=""
											disabled={ subscriptionBlocked() }
											value={ inputTokenId }
											onChange={(e) => {
												setInputTokenId( e.target.value.toLowerCase().replace(/[^0-9-—,]/g, "") )
											}}
										/>
									</div>
									<div className="col col-12 col-md-3">
										<label className="input-label">&nbsp;</label>
										<button
											className="btn btn-grad"
											disabled={ !!addRecipientDisabled() }
											onClick={(e) => {
												addRecipientRow(inputContractAddress, inputTokenId);
											}}
										>Add</button>
									</div>
								</div>

								<div className="c-wrap__upload-row">
									<button
										className="btn-link btn-csv mr-md-3"
										onClick={ open }
									>Click to upload a CSV file</button>
									<span className="d-none d-md-inline">
										<span className="mr-3">or</span>
										drag and drop it here
									</span>
								</div>

							</div>
							<div className="c-wrap__table mt-3">
								{
									recipients.map((item, idx) => { return getRecipientRow(item, idx + 1) })
								}
							</div>
						</div>
					)
				}
			</Dropzone>
		)
	}

	const addCollateralRow = (addressInput: string, amountInput: string) => {

		if ( !currentChain ) { return; }

		let amount = new BigNumber(amountInput);
		if ( amount.isNaN() ) { return; }

		if ( addressInput === '' || addressInput === '0' || addressInput === '0x0000000000000000000000000000000000000000' ) {
			addressInput = '0x0000000000000000000000000000000000000000';
			amount = tokenToInt(amount, currentChain.decimals);
			if ( amount.gt(balanceNative) ) { return; }
		} else {

			let foundERC20 = erc20List.find((item) => {
				return item.contractAddress.toLowerCase() === addressInput.toLowerCase()
			});
			if ( !foundERC20 ) {
				requestERC20Token(addressInput, userAddress);
				foundERC20 = getNullERC20(addressInput);
			}
			if ( foundERC20 ) {
				amount = tokenToInt(amount, foundERC20.decimals || currentChain.decimals);
				if ( amount.gt(foundERC20.balance) ) { return; }
			}
		}

		setCollaterals((prevState) => {
			let collateralsUpdated = prevState.filter((item) => { return item.amount && !item.amount.eq(0) });
			const foundExisting = collateralsUpdated.find((item) => { return item.contractAddress.toLowerCase() === addressInput.toLowerCase() });
			let collateralToAdd: CollateralItem;

			if ( foundExisting ) {
				collateralToAdd = {
					assetType: foundExisting.assetType,
					contractAddress: foundExisting.contractAddress,
					amount: foundExisting.amount ? foundExisting.amount.plus(amount) : amount,
				}
			} else {
				collateralToAdd = {
					assetType: addressInput === '0x0000000000000000000000000000000000000000' ? _AssetType.native : _AssetType.ERC20,
					contractAddress: addressInput,
					amount: amount,
				}
			}

			collateralsUpdated = collateralsUpdated.filter((item) => { return item.contractAddress.toLowerCase() !== addressInput.toLowerCase() });
			return [
				...collateralsUpdated,
				collateralToAdd
			]
		})

		setInputCollateralAddress('');
		setInputCollateralAmount('');
	}
	const addCollateralDisabled = () => {
		if ( subscriptionBlocked() ) { return 'Add' }

		if ( collaterals.length + 1 > maxCollaterals ) {
			return 'Max collaterals'
		}

		if (
			inputCollateralAddress === '' ||
			!Web3.utils.isAddress(inputCollateralAddress)
		) { return 'Wrong address' }

		const amount = new BigNumber(inputCollateralAmount);
		if (
			!inputCollateralAmount ||
			amount.eq(0) ||
			amount.isNaN()
		) { return 'Empty amount' }

		return ''
	}
	const getCollateralAmountTitle = () => {

		if (
			inputCollateralAddress &&
			inputCollateralAddress !== '' &&
			inputCollateralAddress !== '0' &&
			inputCollateralAddress !== '0x0000000000000000000000000000000000000000'
		) {
			let foundERC20 = erc20List.find((item) => {
				if ( !item.contractAddress ) { return false; }
				return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase()
			});
			if ( !foundERC20 ) {
				if ( Web3.utils.isAddress(inputCollateralAddress) ) { requestERC20Token(inputCollateralAddress, userAddress); }

				return (
					<label className="input-label text-orange">
						<TippyWrapper
							msg="decimals is unknown; enter amount in wei"
						>
							<span>Amount*</span>
						</TippyWrapper>
						<TippyWrapper
							msg="Amount of tokens for every recipient"
						>
							<span className="i-tip ml-1"></span>
						</TippyWrapper>
					</label>
				)
			}
		}

		return (
			<label className="input-label">
				Amount
				<TippyWrapper
					msg="Amount of tokens for every recipient"
					elClass="ml-1"
				/>
			</label>
		)
	}
	const collateralOnDrop = (files: Array<File>) => {

		if ( subscriptionBlocked() ) { return; }

		files.forEach((item) => {
			const fileReader = new FileReader();
			fileReader.addEventListener('load', (e) => {
				let text = e.target?.result;
				if ( typeof(text) !== 'string' ) {
					return;
				}
				if ( !text ) { return; }
				let rows: Array<string> = [];
				if ( text.includes('\r\n') ) {
					rows = text.split('\r\n');
				}
				else {
					if ( text.includes('\n\r') ) {
						rows = text.split('\n\r');
					} else {
						if ( text.includes('\n') ) {
							rows = text.split('\n');
						}
					}
				}

				rows.forEach((iitem) => {
					try {
						const address = iitem.split(';')[0];
						const amount = iitem.split(';')[1].replaceAll(',', '.');

						addCollateralRow( address, amount )
					} catch(e) { console.log('Cannot add row from file', iitem) }
				});

			});
			fileReader.readAsText(item)
		})
	}
	const getCollateralERC20Balance = () => {
		if ( !userAddress ) { return null; }
		if ( inputCollateralAddress === '' ) { return null; }

		const getNativeAmount = () => {
			const DECIMALS_TO_SHOW = 5;

			const amount = tokenToFloat(balanceNative, currentChain?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								setInputCollateralAmount( amount.toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => {
						setInputCollateralAmount( amount.toString() )
					}}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		if ( inputCollateralAddress === '0x0000000000000000000000000000000000000000' ) {
			return (
				<div className="c-add__max mt-2 mb-0">
					<div className="mt-1 mb-1">
						<span>Max: </span>
						{ getNativeAmount() }
					</div>
				</div>
			)
		}

		let foundToken = erc20List.find((item) => { return item.contractAddress.toLowerCase() === inputCollateralAddress.toLowerCase() });
		if ( !foundToken ) {
			if ( Web3.utils.isAddress(inputCollateralAddress) ) { requestERC20Token(inputCollateralAddress, userAddress); }
			return;
		}

		const allowance = foundToken.allowance.find((item) => { return item.allowanceTo.toLowerCase() === inputCollateralAddress.toLowerCase() });

		const getERC20Amount = () => {
			const DECIMALS_TO_SHOW = 5;

			if ( !foundToken?.balance ) { return null; }
			const amount = tokenToFloat(foundToken.balance, foundToken?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								if ( !foundToken ) { return; }
								setInputCollateralAmount( tokenToFloat(foundToken.balance, foundToken?.decimals || 18).toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => { setInputCollateralAmount(amount.toString()) }}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}
		const getERC2Allowance = () => {
			const DECIMALS_TO_SHOW = 5;

			if ( !allowance?.amount ) { return null; }

			const amount = tokenToFloat(allowance.amount, foundToken?.decimals || 18);

			if ( amount.eq(0) ) {
				return (
					<button>0</button>
				)
			}

			if ( amount.lt(10**-DECIMALS_TO_SHOW) ) {
				return (
					<TippyWrapper msg={ amount.toString() }>
						<button
							onClick={() => {
								if ( !foundToken ) { return; }
								setInputCollateralAmount( amount.toString() )
							}}
						>&lt;{ new BigNumber(10**-DECIMALS_TO_SHOW).toFixed(DECIMALS_TO_SHOW) }</button>
					</TippyWrapper>
				)
			}
			return (
				<button
					onClick={() => {
						if ( !foundToken ) { return; }
						setInputCollateralAmount( amount.toString() )
					}}
				>{ amount.toFixed(DECIMALS_TO_SHOW) }</button>
			)
		}

		return (
			<div className="c-add__max mt-2 mb-0">
				<div className="mt-1 mb-1">
					<span>Max: </span>
					{ getERC20Amount() }
				</div>
				{
					allowance ? (
						<div>
							<span>Allowance: </span>
							{ getERC2Allowance() }
						</div>
					) : null
				}
			</div>
		)
	}
	const getCollateralBlock = () => {
		if ( !currentChain ) { return null; }

		return (
			<Dropzone
				onDrop={(files) => { collateralOnDrop(files) }}
				accept={{ 'text/csv': ['.txt', '.csv'] }}
				noClick={ true }
				noKeyboard={ true }
			>
				{
					({
						getRootProps, getInputProps, isDragActive, open
					}) => (
						<div {...getRootProps({ className: `c-wrap c-b-wrap ${isDragActive && !subscriptionBlocked() ? 'file-dragged' : ''}` })}>

							<div className="upload-poopover">
								<div className="inner">
									<div className="h3">Drop your file here</div>
								</div>
							</div>

							<input {...getInputProps()} />
							<div className="c-wrap__header" ref={ scrollable.collaterals }>
								<div className="h3">Collateral for each wNFT</div>
								<div className="c-wrap__info">
									<div>Enter <span className="text-grad">address = 0</span> to add { currentChain.symbol }</div>
									<img src={ icon_i_attention } alt="" />
								</div>
							</div>
							<div className="c-wrap__form">
								<div className="row">
									<div className="col col-12 col-md-5">
										<label className="input-label">{ currentChain.EIPPrefix }-20 Address</label>
										<div className={ `select-group ${ subscriptionBlocked() ? 'disabled' : '' }` } >
											<input
												className="input-control"
												type="text"
												placeholder="0x000"
												disabled={ subscriptionBlocked() }
												value={ inputCollateralAddress }
												onChange={(e) => {
													const address = e.target.value.toLowerCase().replace(/[^a-f0-9x]/g, "");
													if ( Web3.utils.isAddress(address) ) {
														let foundERC20 = erc20List.find((item) => {
															if ( !item.contractAddress ) { return false; }
															return item.contractAddress.toLowerCase() === address.toLowerCase()
														});
														if ( !foundERC20 ) {
															foundERC20 = getNullERC20(address);
														}
														requestERC20Token(address, userAddress);
													}
													setInputCollateralAddress(address);
												}}
												onKeyPress={(e) => {
													if ( e.defaultPrevented)         { return; }
													if ( !!addCollateralDisabled() ) { return; }
													if ( e.key !== 'Enter' )         { return; }

													addCollateralRow( inputCollateralAddress, inputCollateralAmount )
												}}
											/>

											<CoinSelector
												tokens        = { filterERC20ContractByPermissions({ enabledForCollateral: true }) }
												selectedToken = { inputCollateralAddress }
												onChange      = {(address: string) => {
													setInputCollateralAddress(address);
												}}
											/>

										</div>
									</div>
									<div className="col col-12 col-md-4">
										{ getCollateralAmountTitle() }
										<input
											className="input-control"
											type="text"
											placeholder=""
											disabled={ subscriptionBlocked() }
											value={ addThousandSeparator(inputCollateralAmount) }
											onChange={(e) => {
												let value = removeThousandSeparator(e.target.value);
												if ( value !== '' && !value.endsWith('.') && !value.endsWith('0') ) {
													if ( new BigNumber(value).isNaN() ) { return; }
													value = new BigNumber(value).toString();
												}
												setInputCollateralAmount(value);
											}}
											onKeyPress={(e) => {
												if ( e.defaultPrevented)         { return; }
												if ( !!addCollateralDisabled() ) { return; }
												if ( e.key !== 'Enter' )         { return; }

												addCollateralRow( inputCollateralAddress, inputCollateralAmount )
											}}
										/>
										{ getCollateralERC20Balance() }
									</div>
									<div className="col col-12 col-md-3">
										<label className="input-label">&nbsp;</label>
										<button
											className="btn btn-grad"
											disabled={ !!addCollateralDisabled() }
											onClick={ (e) => {
												addCollateralRow( inputCollateralAddress, inputCollateralAmount )
											} }
										>Add</button>
									</div>
								</div>

								<div className="c-wrap__upload-row">
									<button
										className="btn-link btn-csv mr-md-3"
										onClick={ open }
									>Click to upload a CSV file</button>
									<span className="d-none d-md-inline">
										<span className="mr-3">or </span>
										drag and drop it here
									</span>
								</div>

							</div>

							<CollateralViewer
								collaterals={ collaterals }
								removeRow={(item: CollateralItem) => {
									setCollaterals([
										...collaterals.filter((iitem) => { return iitem.contractAddress.toLowerCase() !== item.contractAddress.toLowerCase() }),
									]);
								}}
								width={ 'wide' }
							/>
							{
								collaterals.length && recipients.length ? (
									<CollateralSummary
										collaterals={ collaterals }
										recipients={ recipients.length }
										rows={3}
										/>
								) : null
							}
						</div>
					)
				}
			</Dropzone>
		)
	}

	const hasErrors = () => {

		const errors: Array<{ msg: string, block: any, showError?: string }> = [];

		// ----- COLLATERALS -----
		if ( !collaterals.length ) { errors.push({ msg: 'Empty collaterals list', block: scrollable.collaterals }) }
		// ----- END COLLATERALS -----

		// ----- RECIPIENTS -----
		if ( !recipients.length ) {
			errors.push({ msg: 'Empty wNFT list', block: scrollable.recipients })
		} else {
			const erroredTokens = loadedWNFTs.find((item) => {
				const tokenAdded = recipients.find((iitem) =>{ return item.address.toLowerCase() === iitem.address.toLowerCase() && item.tokenId === iitem.tokenId });
				if ( !tokenAdded ) { return false; }
				if ( !item.status || item.status === 'pending' ) { return false; }
				return true;
			});
			if ( erroredTokens ) {
				errors.push({ msg: 'There are problems with tokens', block: scrollable.recipients })
			}

			if ( !erroredTokens ) {
				const wrongWrapperTokens = loadedWNFTs.find((item) => {
					const tokenAdded = recipients.find((iitem) =>{ return item.address.toLowerCase() === iitem.address.toLowerCase() && item.tokenId === iitem.tokenId });
					if ( !tokenAdded ) { return false; }
					if ( !wrapperContract ) { return false; }
					if ( !item.trustedWrapper ) { return false; }

					if ( item.trustedWrapper.toLowerCase() !== wrapperContract.toLowerCase() ) { return true; }

					return false;
				});
				if ( wrongWrapperTokens ) {
					errors.push({ msg: 'There are tokens with unsupported wrapper', block: scrollable.recipients })
				}
			}
		}
		// ----- END RECIPIENTS -----

		// collateralErrors.forEach((item) => { errors.push({ msg: `Error with collateral: ${compactString(item.address)} - ${item.tokenId || ''}. ${item.msg}`, block: scrollablecollaterals }) })

		return errors;
	}
	const getErrorsBlock = () => {
		const errors = hasErrors();

		if ( !errors.length ) { return null; }

		return (
			<div className="c-errors">
				<div className="mb-3">To enable the Wrap button, correct the following errors:</div>
				<ul>
				{
					errors.map((item) => {
						return (
							<li key={ item.msg }>
								<button
									className="btn-link"
									onClick={() => {
										item.block.current.scrollIntoView();
										if ( item.showError ) {
											setShowErrors([
												...showErrors.filter((iitem) => { return iitem !== item.showError }),
												item.showError
											]);
										}
									}}
								>
									{ item.msg }
								</button>
							</li>
						)
					})
				}
				</ul>
			</div>
		)
	}
	const getClearBtn = () => {
		return (
			<button
				className="btn btn-yellow w-100"
				onClick={() => {
					setRecipients([]);
					setCollaterals([]);
					setInputCollateralAddress('');
					setInputCollateralAmount('');
				}}
			>Clear form</button>
		)
	}

	const createAdvancedLoaderAddCollateral = (_currentChain: ChainType, _web3: Web3, _userAddress: string) => {
		const loaderStages: Array<AdvancedLoaderStageType> = []

		if ( collaterals.length ) {
			loaderStages.push({
				id: 'approve20collateral',
				sortOrder: 10,
				text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
				status: _AdvancedLoadingStatus.queued,
				current: 0,
				total: collaterals.length,
			});
		}

		loaderStages.push({
			id: 'addcoll',
			sortOrder: 20,
			text: 'Adding tokens to collateral',
			status: _AdvancedLoadingStatus.queued
		});

		const advLoader = {
			title: 'Waiting to add',
			stages: loaderStages
		};
		createAdvancedLoader(advLoader);
	}
	const approve20Collateral = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		if ( !batchWorkerContract ) { return; }

		const erc20CollateralsToCheck = collaterals.filter((item) => { return item.assetType === _AssetType.ERC20 });
		if ( !erc20CollateralsToCheck.length ) { return; }

		for (let idx = 0; idx < erc20CollateralsToCheck.length; idx++) {
			const item = erc20CollateralsToCheck[idx];
			let foundToken = erc20List.find((iitem) => { return iitem.contractAddress.toLowerCase() === item.contractAddress.toLowerCase() });
			if ( !foundToken ) {
				foundToken = getNullERC20(item.contractAddress);
			}

			updateStepAdvancedLoader({
				id: 'approve20collateral',
				status: _AdvancedLoadingStatus.loading,
				text: `Approving ${ foundToken.symbol }`,
				current: idx + 1,
			});

			if ( !item.amount ) { continue; }
			if ( item.contractAddress.toLowerCase() === techToken.toLowerCase() ) { continue; }

			const amountToCheck = item.amount.multipliedBy(recipients.length);

			const balance = await getERC20BalanceFromChain(_currentChain.chainId, item.contractAddress, _userAddress, batchWorkerContract);
			if ( balance.balance.lt(amountToCheck) ) {
				setModal({
					type: _ModalTypes.error,
					title: `Not enough ${foundToken.symbol}`,
					details: [
						`Token ${foundToken.symbol}: ${item.contractAddress}`,
						`User address: ${_userAddress}`,
						`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
					]
				});
				throw new Error();
			}
			if ( !balance.allowance || balance.allowance.amount.lt(amountToCheck) ) {
				try {
					if ( isMultisig ) {
						await makeERC20AllowanceMultisig(_web3, item.contractAddress, _userAddress, amountToCheck, batchWorkerContract);
					} else {
						await makeERC20Allowance(_web3, item.contractAddress, _userAddress, amountToCheck, batchWorkerContract);
					}
				} catch(e: any) {
					setModal({
						type: _ModalTypes.error,
						title: `Cannot approve ${foundToken.symbol}`,
						details: [
							`Token ${foundToken.symbol}: ${item.contractAddress}`,
							`User address: ${_userAddress}`,
							`User balance: ${tokenToFloat(balance.balance, foundToken.decimals).toString()}`,
							'',
							e.message || e,
						]
					});
					throw new Error();
				}
			}

		}

		updateStepAdvancedLoader({
			id: 'approve20collateral',
			text: `Approving ${_currentChain.EIPPrefix}-20 collateral tokens`,
			status: _AdvancedLoadingStatus.complete
		});

	}
	const addCollSubmit = async (_currentChain: ChainType, _web3: Web3, _userAddress: string, isMultisig?: boolean) => {

		if ( !batchWorkerContract ) { return; }

		createAdvancedLoaderAddCollateral(_currentChain, _web3, _userAddress);
		try { await approve20Collateral(_currentChain, _web3, _userAddress, isMultisig); } catch(ignored) { return; }

		updateStepAdvancedLoader({
			id: 'addcoll',
			status: _AdvancedLoadingStatus.loading
		});

		let txResp;
		try {
			if ( isMultisig ) {
				txResp = await addCollateralBatchMultisig(
					_web3,
					batchWorkerContract,
					recipients.map((item) => { return { address: item.address, tokenId: item.tokenId } }),
					collaterals
				);
			} else {
				txResp = await addCollateralBatch(
					_web3,
					batchWorkerContract,
					recipients.map((item) => { return { address: item.address, tokenId: item.tokenId } }),
					collaterals
				);
			}
		} catch(e: any) {
			setModal({
				type: _ModalTypes.error,
				title: `Cannot wrap token`,
				details: [
					`Batch worker contract ${batchWorkerContract}`,
					`User address: ${_userAddress}`,
					`WNFTs: ${JSON.stringify(recipients)}`,
					`Collaterals: ${JSON.stringify(collaterals)}`,
					'',
					e.message || e,
				]
			});
			return;
		}

		updateStepAdvancedLoader({
			id: 'addcoll',
			status: _AdvancedLoadingStatus.complete
		});
		unsetModal();
		updateTicket();

		if ( isMultisig ) {
			setModal({
				type: _ModalTypes.success,
				title: `Transactions have been created`,
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setRecipients([]);
						setCollaterals([]);
						setInputCollateralAddress('');
						setInputCollateralAmount('');
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		} else {
			setModal({
				type: _ModalTypes.success,
				title: `Value successfully added`,
				text: currentChain?.hasOracle ? [
					{ text: 'After oracle synchronization, your changes will appear on dashboard soon. Please refresh the dashboard page' },
				] : [],
				buttons: [{
					text: 'Ok',
					clickFunc: () => {
						unsetModal();
						setRecipients([]);
						setCollaterals([]);
						setInputCollateralAddress('');
						setInputCollateralAmount('');
					}
				}],
				links: [{
					text: `View on ${_currentChain.explorerName}`,
					url: combineURLs(_currentChain.explorerBaseUrl, `/tx/${txResp.transactionHash}`)
				}],
			});
		}

	}

	const getSubmitBtn = () => {
		return (
			<button
				className="btn btn-lg w-100"
				disabled={ !!hasErrors().length }
				onClick={async () => {
					if ( !currentChain ) { return; }
					let _web3 = web3;
					let _userAddress = userAddress;
					let _currentChain = currentChain;

					if ( !web3 || !userAddress || await getChainId(web3) !== _currentChain.chainId ) {
						setLoading('Waiting for wallet');

						try {
							const web3Params = await getWeb3Force(_currentChain.chainId);

							_web3 = web3Params.web3;
							_userAddress = web3Params.userAddress;
							_currentChain = web3Params.chain;
							unsetModal();
						} catch(e: any) {
							setModal({
								type: _ModalTypes.error,
								title: 'Error with wallet',
								details: [
									e.message || e
								]
							});
							return;
						}
					}

					if ( !_web3 || !_userAddress || !_currentChain ) { return; }
					const isMultisig = localStorageGet('authMethod').toLowerCase() === 'safe';
					addCollSubmit(_currentChain, _web3, _userAddress, isMultisig);
				}}
			>Add collateral</button>
		)
	}

	return (
		<>
			<div className="col-12 col-sm-12 mb-2 mb-sm-0">
				<p className="text-orange">Only for SAFT created wNFT</p>
			</div>
			{ getRecipientsBlock() }
			{ getCollateralBlock() }

			<div className="row mb-7 mb-lg-9">
				<div className="col-md-5 py-3 order-md-2">
					{ getErrorsBlock() }
				</div>
				<div className="col-md-4 py-3 order-md-3">
					{ getSubmitBtn() }
				</div>
				<div className="col-md-3 py-3 order-md-1">
					{ getClearBtn() }
				</div>
			</div>
		</>
	)

}