
import {
	useContext
} from 'react';

import default_token_preview from '../../static/pics/tb-nft-default.svg';
import icon_loading          from '../../static/pics/loading.svg';
import default_icon          from '@envelop/envelop-client-core/static/pics/coins/_default.svg';
import {
	BigNumber,
	CollateralItem,
	SAFTRecipientItem,
	_AssetType,
	compactString,
	getNullERC20,
	tokenToFloat
} from '@envelop/envelop-client-core';
import {
	ERC20Context,
	Web3Context
} from '../../dispatchers';

export type CollateralError = {
	contractAddress: string,
	tokenId?: string,
	msg: string,
}

type CollateralSummaryProps = {
	collaterals         : Array<CollateralItem>,
	recipients          : number,
	rows                : number,
	nonRemovableAddress?: string,
}

export default function CollateralSummary(props: CollateralSummaryProps) {

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		erc20List,
		requestERC20Token,
	} = useContext(ERC20Context);

	const {
		collaterals,
		rows,
		nonRemovableAddress,
		recipients
	} = props;

	const getCollateralNativeRow = (item: CollateralItem) => {

		if ( !currentChain ) { return null; }
		if ( !item.amount ) { return null; }

		return (
			<tr key={ `${item.contractAddress}` }>
				<td>
					<div className="tb-coin">
						<span className="i-coin">
							<img src={ currentChain.tokenIcon || default_icon } alt="" />
						</span>
						<span>
							{ currentChain.symbol }
							{ ' ' }
						</span>
					</div>
				</td>
				<td>{ tokenToFloat(item.amount.multipliedBy(recipients), currentChain?.decimals || 18).toString() }</td>
			</tr>
		)
	}
	const getCollateralERC20Row = (item: CollateralItem) => {

		if ( !currentChain ) { return null; }
		if ( !item.amount ) { return null; }

		let foundToken = erc20List.find((iitem) => {
			if ( !iitem.contractAddress ) { return false; }
			return item.contractAddress.toLowerCase() === iitem.contractAddress.toLowerCase()
		});
		if ( !foundToken ) {
			requestERC20Token(item.contractAddress);
			foundToken = getNullERC20(item.contractAddress);
		}

		return (
			<tr key={ `${item.contractAddress}` }>
				<td>
					<div className="tb-coin">
						<span className="i-coin">
							<img src={ foundToken.icon || default_icon } alt="" />
						</span>
						<span>
							{ foundToken.symbol }
							{ ' ' }
						</span>
					</div>
				</td>
				<td>{ tokenToFloat(item.amount.multipliedBy(recipients), foundToken.decimals || 18).toString() }</td>
			</tr>
		)
	}
	const getNFTPreview = (item: CollateralItem) => {
		if ( item.tokenImg === undefined ) {
			return (
				<img src={ icon_loading } alt="" />
			)
		}
		if ( item.tokenImg === '' ) {
			return ( <img src={ default_token_preview } alt="" /> )
		}

		return (
			<img src={ item.tokenImg } alt="" />
		)
	}
	const getCollateralERC721Row = (item: CollateralItem) => {
		if ( !currentChain ) { return null; }

		return (
			<tr key={ `${item.contractAddress}${item.tokenId}` }>
				<td>
					<div className="tb-nft">
						<span className="i-coin">
							{ getNFTPreview(item) }
						</span>
						<span>
							({ currentChain.EIPPrefix }-721) { compactString(item.contractAddress) }
						</span>
					</div>
				</td>
				<td>1</td>
			</tr>
		)
	}
	const get1155Amount = (qty: BigNumber | undefined) => {
		if ( !qty ) { return '' }
		if ( qty.eq(0) ) { return '' }
		if ( qty.eq(1) ) { return '1 item' }
		return `${qty} items`
	}
	const getCollateralERC1155Row = (item: CollateralItem) => {
		if ( !currentChain ) { return null; }
		if ( !item.amount ) { return null; }

		return (
			<tr key={ `${item.contractAddress}${item.tokenId}` }>
				<td>
					<div className="tb-nft">
						<span className="i-coin">
							{ getNFTPreview(item) }
						</span>
						<span>
							({ currentChain.EIPPrefix }-1155) { compactString(item.contractAddress) }
						</span>
					</div>
				</td>
				<td>{ get1155Amount(item.amount.multipliedBy(recipients)) }</td>
			</tr>
		)
	}

	return (
		<div className={ `c-wrap__summary` }>
			<div className="h5 text-muted">Collateral Summary</div>
			<div className="row">
			{
				collaterals
				.sort((item, prev) => {
					if (
						nonRemovableAddress &&
						item.contractAddress.toLowerCase() === nonRemovableAddress.toLowerCase()
					) { return -1; }
					if (
						nonRemovableAddress &&
						prev.contractAddress.toLowerCase() === nonRemovableAddress.toLowerCase()
					) { return 1; }

					if ( item.assetType < prev.assetType ) { return -1 }
					if ( item.assetType > prev.assetType ) { return  1 }

					if ( item.contractAddress.toLowerCase() < prev.contractAddress.toLowerCase() ) { return -1 }
					if ( item.contractAddress.toLowerCase() > prev.contractAddress.toLowerCase() ) { return  1 }

					if ( item.tokenId && prev.tokenId ) {
						try {
							if ( new BigNumber(item.tokenId).isNaN() || new BigNumber(prev.tokenId).isNaN() ) {
								if ( parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`) ) { return -1 }
								if ( parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`) ) { return  1 }
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if ( itemTokenIdNumber.lt(prevTokenIdNumber) ) { return -1 }
							if ( itemTokenIdNumber.gt(prevTokenIdNumber) ) { return  1 }
						} catch ( ignored ) {
							if ( `${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase() ) { return -1 }
							if ( `${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase() ) { return  1 }
						}
					}

					return 0
				})
				.reduce((acc: Array<Array<CollateralItem>>, _, i, array) => {
					if (i % rows === 0) { acc.push( array.slice(i, i + rows) ) }
					return acc
				}, [])
				.map((item, idx) => {
					return (
						<div className="col-12 col-sm-6 col-md-4 col-lg-3" key={ idx }>
							<table>
								<tbody>
									{
										item.map((iitem) => {
											if ( iitem.assetType === _AssetType.native  ) { return getCollateralNativeRow(iitem)  }
											if ( iitem.assetType === _AssetType.ERC20   ) { return getCollateralERC20Row(iitem)   }
											if ( iitem.assetType === _AssetType.ERC721  ) { return getCollateralERC721Row(iitem)  }
											if ( iitem.assetType === _AssetType.ERC1155 ) { return getCollateralERC1155Row(iitem) }

											return null;
										})
									}
								</tbody>
							</table>
						</div>
					)
				})
			}
			</div>
		</div>
	)
}