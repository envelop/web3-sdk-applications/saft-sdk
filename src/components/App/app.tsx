
import { useContext, useEffect, useState } from 'react';

import Header from '../Header';
import Footer from '../Footer';

import {
	InfoModalContext,
	SubscriptionDispatcher,
	Web3Context,
} from '../../dispatchers';
import SAFTPage from '../SAFTPage';
import config from '../../app.config.json';
import InfoMessages from '../InfoMessages';


export default function App() {

	const [ saftContract, setSaftContract ] = useState('');

	const {
		currentChain,
	} = useContext(Web3Context);
	const {
		setError,
	} = useContext(InfoModalContext);

	useEffect(() => {
		if ( !currentChain ) { setSaftContract(''); return; }

		const foundChainData = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === currentChain.chainId });
		if ( !foundChainData ) {
			setError('Unsupported chain');
			return;
		}

		if ( foundChainData.batchWorker ) { setSaftContract(foundChainData.batchWorker) }
	}, [ currentChain ]);

	return (
		<>
			<InfoMessages />
			<Header />

			<SubscriptionDispatcher
				TX_names={{ singular: 'wrap', plural: 'wraps' }}
				serviceContract={ saftContract }
			>
				<SAFTPage />
			</SubscriptionDispatcher>

			<Footer />
		</>
	)

}