
export type APIOracleSign = {
	oracle_signature: string,
	oracle_signer: string,
	sender: string,
	source_chain: number,
	source_keeper: string,
	target_chain: number,
	target_contract: string,
	target_token_id: string,
	tx_hash: string,
}

export type SwarmStampBatchId = {
	name: string,
	desc: string,
}


export const fetchSwarmStamp = async (params: {
	name: string,
	desc: string,
	image: string | ArrayBuffer,
	mime: string,
	props: Array<{
		type: string,
		name: string
	}>
}):Promise<Array<SwarmStampBatchId> | undefined> => {

	let BASE_URL = process.env.REACT_APP_ORACLE_API_MINT_URL;
	let url = BASE_URL || '';
	if ( !BASE_URL ) {
		BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
		if ( !BASE_URL ) { console.log('No oracle mint url in .env'); return undefined; }
		url = combineURLs(BASE_URL, `mint/`);
	}
	url  = combineURLs(url, `new/`);
	console.log('url', url);

	let respParsed: Array<SwarmStampBatchId>;

	try {

		// set timeout for fetch request
		const controller = new AbortController();
		setTimeout(() => { controller.abort(); console.log("Fetch request 10s timeout occurred"); }, 10000);

		const requestOptions = {
	        method: 'POST',
	        signal: controller.signal,
	        headers: {
	        	// 'Authorization': authToken,
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	        	name: params.name,
	        	desc: params.desc,
	        	image: params.image,
	        	mime: params.mime,
	        	props: params.props
	        })
	    };
		const resp = await fetch(url, requestOptions);
		respParsed = await resp.json();
	} catch (e) {
		console.log('Cannot post newly minted NFT data', e);
		return undefined;
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	return respParsed;
}

export const getOracleNftMinterSign = async (params: {
	address: string,
	chain_id: number,
	token_id: number,
	token_uri: string,
	batch: number,
	amount: number,
	standart: number
}):Promise<APIOracleSign | undefined> => {

	let BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) { console.log('No oracle base url in .env'); return undefined; }

	const url  = combineURLs(BASE_URL, '/web3/mintsign');

	let respParsed: APIOracleSign;

	try {
	    const requestOptions = {
	        method: 'POST',
	        headers: {
	        	// 'Authorization': authToken,
	        	'Content-Type': 'application/json'
	        },
	        body: JSON.stringify({
	        	address: params.address,
	        	chainId: params.chain_id,
	        	tokenId: params.token_id.toString(),
	        	tokenURI: params.token_uri,
	        	batch: params.batch,
	        	amount: params.amount,
	        	standart: params.standart
	        })
	    };
		const resp = await fetch(url, requestOptions);
		respParsed = await resp.json();
	} catch (e) {
		console.log('Cannot fetch oracle signature', e);
		return undefined;
	}

	if ( !respParsed || 'error' in respParsed ) { return undefined; }

	return respParsed;
}

export const getABI = (chainId: number, typeName: string) => {
	let ABI = undefined;

	try { ABI = require(`./abis/${typeName || ''}.json`) } catch(ignored) {}
	if ( ABI ) { return ABI }

	throw new Error(`Cannot load ${chainId}/${typeName} abi`);
}

import Web3         from 'web3';
import { Contract } from "web3-eth-contract";

import BigNumber from 'bignumber.js';
import { combineURLs } from '@envelop/envelop-client-core';
BigNumber.config({ DECIMAL_PLACES: 50, EXPONENTIAL_AT: 100});

type NftMinterPropsType = {
	chainId            : number,
	web3               : Web3,
	contract721Address : string,
	contract1155Address: string,
	userAddress        : string,
}

export class NftMinterContract {

	chainId            : number;
	web3               : Web3;
	contract721Address : string;
	contract1155Address: string;
	userAddress        : string;
	contract721!       : Contract;
	contract1155!      : Contract;

	constructor(props: NftMinterPropsType) {
		this.chainId             = props.chainId;
		this.web3                = props.web3;
		this.contract721Address  = props.contract721Address;
		this.contract1155Address = props.contract1155Address;
		this.userAddress         = props.userAddress;

		console.log('this.contract721Address', this.contract721Address);
		let nftMinter721ABI;
		try {
			nftMinter721ABI = getABI(this.chainId, 'nftminter721');
		} catch(e) {
			console.log(`Cannot load ${this.contract721Address} NFT minter 721 abi:`, e);
			throw new Error(`Cannot load NFT minter 721 abi`);
		}
		this.contract721 = new this.web3.eth.Contract(nftMinter721ABI, this.contract721Address);
		console.log('NFT minter 721', this.contract721);

		console.log('this.contract1155Address', this.contract1155Address);
		let nftMinter1155ABI;
		try {
			nftMinter1155ABI = getABI(this.chainId, 'nftminter1155');
		} catch(e) {
			console.log(`Cannot load ${this.contract1155Address} NFT minter 1155 abi:`, e);
			throw new Error(`Cannot load NFT minter 1155 abi`);
		}
		this.contract1155 = new this.web3.eth.Contract(nftMinter1155ABI, this.contract1155Address);
		console.log('NFT minter 1155', this.contract1155);

	}

	async getContractName() {
		const output = await this.contract721.methods.name().call();
		return output;
	}

	async getTotalSupply1155() {
		let lastId = 0;
		let t = 0;
		while(true) {
			t++;
			const supply = await this.contract1155.methods.totalSupply(t).call();
			if(supply < 1) {
				lastId = t - 1;
				break;
			}
		}
		return lastId;
	}

	async getTotalSupply(params: {
		standart: number
	}) {
		let output = 0
		if ( params.standart === 721 ) {
			output = await this.contract721.methods.totalSupply().call();
		}
		else if ( params.standart === 1155 ) {
			output = await this.getTotalSupply1155();
		}
		return output;
	}

	async mintWithURI(params: {
		tokenId: number,
		tokenURI: string,
		batch: number,
		amount: number,
		standart: number,
		oracleSignature: string,
		userAddress: string
	}) {

		let tx = ( params.standart === 721 ) ? this.contract721.methods.mintWithURI(params.userAddress, params.tokenId, params.tokenURI, params.oracleSignature) : this.contract1155.methods.mintWithURI(params.userAddress, params.tokenId, params.amount, params.tokenURI, params.oracleSignature);

		if ( params.batch > 1 ) {
			// Prepare batch
			let addresses = [];
			let tokenIds = [];
			let tokenURIs = [];
			let amounts = [];
			let oracleSignature = params.oracleSignature;
			for ( let i = 0; i < params.batch; i++ ) {
				addresses.push(params.userAddress as any);
				tokenIds.push(params.tokenId++ as any);
				tokenURIs.push(params.tokenURI as any);
				amounts.push(params.amount as any);
			}
			let signatures = oracleSignature.split(';');
			tx = ( params.standart === 721 ) ? this.contract721.methods.mintWithURIBatch(addresses,tokenIds,tokenURIs,signatures) : this.contract1155.methods.mintWithURIBatch(addresses,tokenIds,amounts, tokenURIs,signatures);
		}

		let estimatedGas;
		let data;

		try {
			estimatedGas = await tx.estimateGas({ from: params.userAddress });
		} catch(e) {
			throw e;
		}

		// slightly adjust gas for Goerli and Polygon
		if([5,137].includes(this.chainId as number)) {
			estimatedGas = new BigNumber(estimatedGas).plus(new BigNumber(100000)).toString();
		}

		data = tx.send({ from: params.userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null });

		return data;
	}

}