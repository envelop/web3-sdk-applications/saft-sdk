import {
	BigNumber,
	CollateralItem,
	Fee,
	Lock,
	Royalty,
	RoyaltyInput,
	Rules,
	SAFTRecipientItem,
	Web3,
	_Asset,
	_AssetItem,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	createContract,
	decodeAssetTypeFromIndex,
	encodeCollaterals,
	encodeFees,
	encodeLocks,
	encodeRoyalties,
	encodeRules,
	getChainId,
	getDefaultWeb3,
	getNativeCollateral,
	getUserAddress
} from "@envelop/envelop-client-core";

import config from '../app.config.json';

export const getMaxCollaterals = async (chainId: number, wrapperAddress: string): Promise<number> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);

	let slots = 25;
	try {
		slots = parseInt(await contract.methods.MAX_COLLATERAL_SLOTS().call());
	} catch(e: any) { throw e; }

	return slots;

}
export const getWrapperWhitelist = async (chainId: number, wrapperAddress: string): Promise<string | undefined> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);
	try {
		const whitelist = await contract.methods.protocolWhiteList().call();
		if ( !whitelist || whitelist === '' || whitelist === '0x0000000000000000000000000000000000000000' ) { return undefined; }
		return whitelist
	} catch(e: any) { throw e; }

}
export const getWhitelist = async (chainId: number, whitelistAddress: string): Promise<Array<_Asset>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const whitelist = await contract.methods.getWLAddresses().call();
		return whitelist.map((item: { contractAddress: string, assetType: string }) => { return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) } });
	} catch(e: any) { throw e; }

}
export const getWhitelistItem = async (chainId: number, whitelistAddress: string, idx: number): Promise<_Asset> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.getWLAddressByIndex(idx).call();
	} catch(e: any) { throw e; }

}
export const getBlacklist = async (chainId: number, whitelistAddress: string): Promise<Array<_Asset>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const blacklist = await contract.methods.getBLAddresses().call();
		return blacklist.map((item: { contractAddress: string, assetType: string }) => { return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) } });
	} catch(e: any) { throw e; }

}
export const isEnabledForCollateral = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForCollateral(tokenAddress).call();
	} catch(e: any) { throw e; }

}
export const isEnabledForFee = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForFee(tokenAddress).call();
	} catch(e: any) { throw e; }

}
export const isEnabledRemoveFromCollateral = async (chainId: number, whitelistAddress: string, tokenAddress: string): Promise<boolean> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledRemoveFromCollateral(tokenAddress).call();
	} catch(e: any) { throw e; }

}

export const saftToken = async (
	web3: Web3,
	batchWorkerContract: string,
	wrapperContract: string,
	params: {
		originalAddress  : _Asset,
		unwrapDestination: string,
		fees             : Array<Fee>,
		locks            : Array<Lock>,
		royalties        : Array<Royalty>,
		rules            : Rules,
		outType          : _AssetType,
		outBalance       : number,
		recipients       : Array<SAFTRecipientItem>,
		collaterals      : Array<CollateralItem>,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'batchWorker', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(params.collaterals).multipliedBy(params.recipients.length);
	const recievers = params.recipients.map((item) => { return item.userAddress });
	let inDataParsed = params.recipients.map((item) => {
		return {
			inAsset: {
				asset: {
					assetType: params.originalAddress.assetType ? params.originalAddress.assetType : _AssetType.empty,
					contractAddress: params.originalAddress.contractAddress ? params.originalAddress.contractAddress : '0x0000000000000000000000000000000000000000'
				},
				tokenId: item.tokenId || '0',
				amount: '0',
			},
			unWrapDestination: params.unwrapDestination,
			fees             : encodeFees(params.fees),
			locks            : encodeLocks(params.locks),
			royalties        : encodeRoyalties(params.royalties, wrapperContract),
			outType          : params.outType,
			outBalance       : params.outBalance.toString(),
			rules            : encodeRules(params.rules),
		}
	});
	const collateralsParsed = params.collaterals
			.map((item) => {
				return {
					asset: {
						assetType: item.assetType,
						contractAddress: item.contractAddress
					},
					tokenId: item.tokenId || '0',
					amount: item.amount ? item.amount.toString() : '0'
				}
			});


	const tx = contract.methods.wrapBatch(
		inDataParsed,
		collateralsParsed,
		recievers
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativeCollateral , maxPriorityFeePerGas: null, maxFeePerGas: null});
}
export const saftTokenMultisig = async (
	web3: Web3,
	batchWorkerContract: string,
	wrapperContract: string,
	params: {
		originalAddress  : _Asset,
		unwrapDestination: string,
		fees             : Array<Fee>,
		locks            : Array<Lock>,
		royalties        : Array<Royalty>,
		rules            : Rules,
		outType          : _AssetType,
		outBalance       : number,
		recipients       : Array<SAFTRecipientItem>,
		collaterals      : Array<CollateralItem>,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'batchWorker', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(params.collaterals).multipliedBy(params.recipients.length);
	const recievers = params.recipients.map((item) => { return item.userAddress });
	let inDataParsed = params.recipients.map((item) => {
		return {
			inAsset: {
				asset: {
					assetType: params.originalAddress.assetType ? params.originalAddress.assetType : _AssetType.empty,
					contractAddress: params.originalAddress.contractAddress ? params.originalAddress.contractAddress : '0x0000000000000000000000000000000000000000'
				},
				tokenId: item.tokenId || '0',
				amount: '0',
			},
			unWrapDestination: params.unwrapDestination,
			fees             : encodeFees(params.fees),
			locks            : encodeLocks(params.locks),
			royalties        : encodeRoyalties(params.royalties, wrapperContract),
			outType          : params.outType,
			outBalance       : params.outBalance.toString(),
			rules            : encodeRules(params.rules),
		}
	});
	const collateralsParsed = params.collaterals
			.map((item) => {
				return {
					asset: {
						assetType: item.assetType,
						contractAddress: item.contractAddress
					},
					tokenId: item.tokenId || '0',
					amount: item.amount ? item.amount.toString() : '0'
				}
			});


	const tx = contract.methods.wrapBatch(
		inDataParsed,
		collateralsParsed,
		recievers
	);

	// try {
	// 	await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	// } catch(e) {
	// 	throw e;
	// }

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}

export const addCollateralBatch = async (
	web3: Web3,
	batchWorkerContract: string,
	recipients: Array<{ address: string, tokenId: string }>,
	collaterals: Array<CollateralItem>,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'batchWorker', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(collaterals).multipliedBy(recipients.length);

	const recievers = recipients.map((item) => { return item.address });
	const tokenIds = recipients.map((item) => { return item.tokenId });

	const collateralsParsed = collaterals
		.map((item) => {
			return {
				asset: {
					assetType: item.assetType,
					contractAddress: item.contractAddress
				},
				tokenId: item.tokenId || '0',
				amount: item.amount ? item.amount.toString() : '0'
			}
		});

	const tx = contract.methods.addCollateralBatch(
		recievers,
		tokenIds,
		collateralsParsed,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativeCollateral , maxPriorityFeePerGas: null, maxFeePerGas: null});
}
export const addCollateralBatchMultisig = async (
	web3: Web3,
	batchWorkerContract: string,
	recipients: Array<{ address: string, tokenId: string }>,
	collaterals: Array<CollateralItem>,
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'batchWorker', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(collaterals).multipliedBy(recipients.length);

	const recievers = recipients.map((item) => { return item.address });
	const tokenIds = recipients.map((item) => { return item.tokenId });

	const collateralsParsed = collaterals
		.map((item) => {
			return {
				asset: {
					assetType: item.assetType,
					contractAddress: item.contractAddress
				},
				tokenId: item.tokenId || '0',
				amount: item.amount ? item.amount.toString() : '0'
			}
		});

	const tx = contract.methods.addCollateralBatch(
		recievers,
		tokenIds,
		collateralsParsed,
	);

	// try {
	// 	await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	// } catch(e) {
	// 	throw e;
	// }

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}

export const getUserBatchWrappers = async (chainId: number, collectionAddress: string, userAddress: string): Promise<Array<_Asset>> => {

	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'batchworkercollection', collectionAddress);
	try {
		const whitelist = await contract.methods.getUsersCollections(userAddress).call();
		return whitelist.map((item: { contractAddress: string, assetType: string }) => { return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) } });
	} catch(e: any) { throw e; }

}

export const deployUserBatchWorker = async (
	web3: Web3,
	collectionAddress: string,
	params: {
		name: string,
		symbol: string,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const chainId = await getChainId(web3);
	if ( !chainId ) { return; }

	const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === chainId });
	if ( !foundChain || !foundChain.userBatchWorkerImplementation || !foundChain.userBatchWorkerWrapper ) {
		throw new Error('No batch worker implementation or wrapper address');
	}

	const defaultBaseUrl = 'https://envelop.is/metadata/';

	const contract = await createContract(web3, 'batchworkercollection', collectionAddress);

	const tx = contract.methods.deployNewCollection(
		foundChain.userBatchWorkerImplementation,
		userAddress,
		params.name,
		params.symbol,
		defaultBaseUrl,
		foundChain.userBatchWorkerWrapper,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null});
}
export const deployUserBatchWorkerMultisig = async (
	web3: Web3,
	collectionAddress: string,
	params: {
		name: string,
		symbol: string,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const chainId = await getChainId(web3);
	if ( !chainId ) { return; }

	const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId === chainId });
	if ( !foundChain || !foundChain.userBatchWorkerImplementation || !foundChain.userBatchWorkerWrapper ) {
		throw new Error('No batch worker implementation or wrapper address');
	}

	const defaultBaseUrl = 'https://envelop.is/metadata/';

	const contract = await createContract(web3, 'batchworkercollection', collectionAddress);

	const tx = contract.methods.deployNewCollection(
		foundChain.userBatchWorkerImplementation,
		userAddress,
		params.name,
		params.symbol,
		defaultBaseUrl,
		foundChain.userBatchWorkerWrapper,
	);

	try {
		await tx.estimateGas({ from: userAddress })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, maxPriorityFeePerGas: null, maxFeePerGas: null});
}

export const saftTokenOnUserContract = async (
	web3: Web3,
	batchWorkerContract: string,
	wrapperContract: string,
	storageContract: string,
	params: {
		originalAddress  : _Asset,
		unwrapDestination: string,
		fees             : Array<Fee>,
		locks            : Array<Lock>,
		royalties        : Array<Royalty>,
		rules            : Rules,
		outType          : _AssetType,
		outBalance       : number,
		recipients       : Array<SAFTRecipientItem>,
		collaterals      : Array<CollateralItem>,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'usertrustedwrapper', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(params.collaterals).multipliedBy(params.recipients.length);
	const recievers = params.recipients.map((item) => { return item.userAddress });
	let inDataParsed = params.recipients.map((item) => {
		return {
			inAsset: {
				asset: {
					assetType: params.originalAddress.assetType ? params.originalAddress.assetType : _AssetType.empty,
					contractAddress: params.originalAddress.contractAddress ? params.originalAddress.contractAddress : '0x0000000000000000000000000000000000000000'
				},
				tokenId: item.tokenId || '0',
				amount: '0',
			},
			unWrapDestination: params.unwrapDestination,
			fees             : encodeFees(params.fees),
			locks            : encodeLocks(params.locks),
			royalties        : encodeRoyalties(params.royalties, wrapperContract),
			outType          : params.outType,
			outBalance       : params.outBalance.toString(),
			rules            : encodeRules(params.rules),
		}
	});
	const collateralsParsed = params.collaterals
			.map((item) => {
				return {
					asset: {
						assetType: item.assetType,
						contractAddress: item.contractAddress
					},
					tokenId: item.tokenId || '0',
					amount: item.amount ? item.amount.toString() : '0'
				}
			});


	const tx = contract.methods.wrapBatch(
		inDataParsed,
		collateralsParsed,
		recievers,
		storageContract
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativeCollateral , maxPriorityFeePerGas: null, maxFeePerGas: null});
}
export const saftTokenOnUserContractMultisig = async (
	web3: Web3,
	batchWorkerContract: string,
	wrapperContract: string,
	storageContract: string,
	params: {
		originalAddress  : _Asset,
		unwrapDestination: string,
		fees             : Array<Fee>,
		locks            : Array<Lock>,
		royalties        : Array<Royalty>,
		rules            : Rules,
		outType          : _AssetType,
		outBalance       : number,
		recipients       : Array<SAFTRecipientItem>,
		collaterals      : Array<CollateralItem>,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'usertrustedwrapper', batchWorkerContract);

	const nativeCollateral = getNativeCollateral(params.collaterals).multipliedBy(params.recipients.length);
	const recievers = params.recipients.map((item) => { return item.userAddress });
	let inDataParsed = params.recipients.map((item) => {
		return {
			inAsset: {
				asset: {
					assetType: params.originalAddress.assetType ? params.originalAddress.assetType : _AssetType.empty,
					contractAddress: params.originalAddress.contractAddress ? params.originalAddress.contractAddress : '0x0000000000000000000000000000000000000000'
				},
				tokenId: item.tokenId || '0',
				amount: '0',
			},
			unWrapDestination: params.unwrapDestination,
			fees             : encodeFees(params.fees),
			locks            : encodeLocks(params.locks),
			royalties        : encodeRoyalties(params.royalties, wrapperContract),
			outType          : params.outType,
			outBalance       : params.outBalance.toString(),
			rules            : encodeRules(params.rules),
		}
	});
	const collateralsParsed = params.collaterals
		.map((item) => {
			return {
				asset: {
					assetType: item.assetType,
					contractAddress: item.contractAddress
				},
				tokenId: item.tokenId || '0',
				amount: item.amount ? item.amount.toString() : '0'
			}
		});



	const tx = contract.methods.wrapBatch(
		inDataParsed,
		collateralsParsed,
		recievers,
		storageContract
	);

	// try {
	// 	await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	// } catch(e) {
	// 	throw e;
	// }

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}